#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
November 2023

Authors:
- Leandro Gimmi

Description:
Map multimodel mean for every returnperiod

"""
import os
# import xesmf as xe
import random
import numpy as np
import xarray as xr 
import pandas as pd
import seaborn as sns
import matplotlib as mpl
# import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
# import cartopy.feature as cfeature
# warnings.simplefilter("ignore")
from matplotlib.lines import Line2D
from matplotlib.patches import Patch

'''-------------------------------------------------------------------------'''

### xHourly, xDaily
time_res = '1hr'

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(2,201,1)

rp_of_interest = [10,20,50,100]
displayed_range = [0,200]

Institutes = ['CLMcom-ETH','CLMcom-BTU','CLMcom-JLU',
              'CLMcom-KIT','KNMI','HCLIMcom','MOHC']
CPMs = ['COSMO-crCLIM','CCLM5-0-14','CCLM5-0-15','CCLM5-0-15',
        'HCLIM38h1-AROME','HCLIM38-AROME','HadREM3-RA-UM10.1']

seasons = ['JJA', 'DJF', 'SON', 'MAM']
title_name = {'rcp85_eoc': 'End of century',
              'rcp85_moc': 'Mid of century',
              'historical': 'Beginning of century'}
### Use combination of regions
# regions = {"CH":1, "CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1, "CHS": 3}
regions = { "CHNE": 1, "CHS": 3 }
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                "SON": 'Autumn'}

path_eoc = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'rcp85/eoc')
path_moc = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'rcp85/moc')
path_hist = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'historical')

d_path_eoc = (f'/scratch/snx3000/lgimmi/store/ALP-3/1d/pr_GEV/'
             +f'rcp85/eoc')
d_path_moc = (f'/scratch/snx3000/lgimmi/store/ALP-3/1d/pr_GEV/'
             +f'rcp85/moc')
d_path_hist = (f'/scratch/snx3000/lgimmi/store/ALP-3/1d/pr_GEV/'
             +f'historical')

if time_res == '1hr':
    plot_path = f'/users/lgimmi/MeteoSwiss/figures/GEV_individual/RPchange/'
elif time_res == '1d':
    plot_path = f'/users/lgimmi/MeteoSwiss/figures/day/RPchange/'

'''-------------------------------------------------------------------------'''
for region in regions.keys():
    print(region)

    sns.set()
    fig, ax = plt.subplots(figsize=(22.0 , 8.0), ncols=4)
    legendset = False

    for i_season, season in enumerate(seasons):
        print(season)
        ### hourly data
        fname_hist = (path_hist + 
                    f'/OneYearStep_EnsembleMean_{time_res}_RP_prmax_{season}.nc')
        fname_moc = (path_moc + 
                    f'/OneYearStep_EnsembleMean_{time_res}_RP_prmax_{season}.nc')
        fname_eoc = (path_eoc + 
                    f'/OneYearStep_EnsembleMean_{time_res}_RP_prmax_{season}.nc')
        
        ds_hist = xr.open_dataset(fname_hist)
        ds_moc = xr.open_dataset(fname_moc)
        ds_eoc = xr.open_dataset(fname_eoc)

        def similar_value(dataset_hist, dataset_moc, dataset_eoc):
            '''
            Find the return period Y of a future dataset that has the closest
              value to the historical value of an Xth return period
            '''
            global return_periods
            da_hist, da_moc, da_eoc = dataset_hist, dataset_moc, dataset_eoc

            hist_list, moc_list, eoc_list = [], [], []
            for i_rp, rp in enumerate(return_periods):

                da_hist = ds_hist[region][i_rp][1]
                da_moc = ds_moc[region][i_rp][1]
                da_eoc = ds_eoc[region][i_rp][1]

                hist_list.append(np.nanmean(da_hist))
                moc_list.append(np.nanmean(da_moc))
                eoc_list.append(np.nanmean(da_eoc))

            similar_moc, similar_eoc = [],[]
            for i_rp, rp in enumerate(return_periods):
                hist_value = hist_list[i_rp]

                idx_moc = min(enumerate(moc_list),
                            key=lambda x: abs(x[1]-hist_value))
                idx_eoc = min(enumerate(eoc_list),
                            key=lambda x: abs(x[1]-hist_value))
                
                bool_check_moc, bool_check_eoc = True, True
                ### Future value has to be in the same range as hist value +- 10%
                if (idx_moc[1] > (hist_value + (hist_value/10)) or 
                    idx_moc[1] < (hist_value - (hist_value/10))):
                    similar_moc.append(rp)
                    bool_check_moc = False
                    # print('moc')
                    
                if (idx_eoc[1] > (hist_value + (hist_value/10)) or 
                    idx_eoc[1] < (hist_value - (hist_value/10))):
                    similar_eoc.append(rp)
                    bool_check_eoc = False
                    # print('eoc')
                    
                ### List with RP that have similar values in future
                if bool_check_moc:
                    similar_moc.append(idx_moc[0]+2)
                if bool_check_eoc:
                    similar_eoc.append(idx_eoc[0]+2)


            
            return similar_moc, similar_eoc , hist_list
        
        similar_moc, similar_eoc, hist_list = similar_value(ds_hist,ds_moc,ds_eoc)

        ds_hist.close()
        ds_moc.close()
        ds_eoc.close()

        ### daily data
        fname_hist = (d_path_hist + 
                    f'/OneYearStep_EnsembleMean_1d_RP_prmax_{season}.nc')
        fname_moc = (d_path_moc + 
                    f'/OneYearStep_EnsembleMean_1d_RP_prmax_{season}.nc')
        fname_eoc = (d_path_eoc + 
                    f'/OneYearStep_EnsembleMean_1d_RP_prmax_{season}.nc')
        
        ds_hist = xr.open_dataset(fname_hist)
        ds_moc = xr.open_dataset(fname_moc)
        ds_eoc = xr.open_dataset(fname_eoc)

        d_similar_moc, d_similar_eoc, d_hist_list = similar_value(ds_hist,ds_moc,ds_eoc)

        ds_hist.close()
        ds_moc.close()
        ds_eoc.close()
        
        ### Set colors for plotting
        if len(rp_of_interest) == 4:
            c_points = ['#EC2323','#AC3FCA', '#5230DE','#217FFE']
            c_points.reverse()
        else:
            random.seed(7)
            c_points = []
            for c in range(len(rp_of_interest)):
                color = random.randrange(0, 2**24)
                hex_color = hex(color)
                c_points.append("#" + hex_color[2:])

        rp_df, rp_df_moc, rp_df_eoc = np.array([]),np.array([]),np.array([])
        d_rp_df, d_rp_df_moc, d_rp_df_eoc = np.array([]),np.array([]),np.array([])

        now_value, moc_value, eoc_value = np.array([]), np.array([]), np.array([])
        d_now_value = np.array([])

        for rpoi in rp_of_interest:
            rpidx = return_periods.tolist().index(rpoi)
            ### hourly data
            rp_df = np.append(rp_df, return_periods[rpidx])
            now_value = np.append(now_value, hist_list[rpidx])
            rp_df_moc = np.append(rp_df_moc, similar_moc[rpidx])
            rp_df_eoc = np.append(rp_df_eoc, similar_eoc[rpidx])
            ### daily data
            d_rp_df = np.append(d_rp_df, return_periods[rpidx])
            d_now_value = np.append(d_now_value, d_hist_list[rpidx])
            d_rp_df_moc = np.append(d_rp_df_moc, d_similar_moc[rpidx])
            d_rp_df_eoc = np.append(d_rp_df_eoc, d_similar_eoc[rpidx])


        # breakpoint()
        ### hourly data
        df = pd.DataFrame(list(zip(rp_df,rp_df_moc,rp_df_eoc)),
                           index=rp_df, columns=[title_name['historical'],
                                                title_name['rcp85_moc'],
                                                title_name['rcp85_eoc']])
        df['colors'] = c_points

        ### daily data
        d_df = pd.DataFrame(list(zip(d_rp_df,d_rp_df_moc,d_rp_df_eoc)),
                           index=d_rp_df,  columns=[title_name['historical'],
                                                    title_name['rcp85_moc'],
                                                    title_name['rcp85_eoc']])
        d_df['colors'] = c_points


        ### --------------------------------------------- 
        def check_in_display_range(val):
            '''
            Checks wheter a value is in displayable range or not, if not it
            returns a patch in black with number of return perod in it.
            This is done to ensure good enough readability of the plot
            
            '''
            global displayed_range, df, d_df, ax, i_season

            if (val==0 and 
                any (df['Beginning of century'].values > displayed_range[1])):
                raise ValueError('Chosen return periods are out of displayable range')
            
            if (val==1 and 
                any (df['Mid of century'].values > displayed_range[1])):
                idx = (np.where(displayed_range[1] < 
                               df['Mid of century'].values)[0])
                for indexi,i in enumerate(idx):
                    ax[i_season].scatter(3, displayed_range[1]- 2 - (indexi*2),
                                        facecolors='none',
                                        edgecolors=c_points[i], s=160)
                    ax[i_season].text(3,  displayed_range[1]- 2 - (indexi*2),
                                      str(int(df['Mid of century'].values[i])),
                                      verticalalignment='center',
                                      horizontalalignment='center',
                                      fontsize=7, weight='bold',color='black')
            
            if (val==2 and 
                any (df['End of century'].values > displayed_range[1])):
                idx = (np.where(displayed_range[1] < 
                               df['End of century'].values)[0])
                for indexi,i in enumerate(idx):
                    ax[i_season].scatter(6, displayed_range[1]- 2 - (indexi*2),
                                        facecolors='none',
                                        edgecolors=c_points[i], s=160)
                    ax[i_season].text(6, displayed_range[1]- 2 - (indexi*2),
                                      str(int(df['End of century'].values[i])),
                                      verticalalignment='center',
                                      horizontalalignment='center',
                                      fontsize=7, weight='bold',color='black')

            if (val==1 and 
                any (d_df['Mid of century'].values > displayed_range[1])):
                idx = (np.where(displayed_range[1] < 
                               d_df['Mid of century'].values)[0])
                for indexi,i in enumerate(idx):
                    ax[i_season].scatter(3, displayed_range[1]- 2 - (indexi*2),
                                        facecolors='none', marker="D",
                                        edgecolors=c_points[i], s=160)
                    ax[i_season].text(3,  displayed_range[1]- 2 - (indexi*2),
                                      str(int(d_df['Mid of century'].values[i])),
                                      verticalalignment='center',
                                      horizontalalignment='center',
                                      fontsize=7, weight='bold',color='black')
            
            if (val==2 and 
                any (d_df['End of century'].values > displayed_range[1])):
                idx = (np.where(displayed_range[1] < 
                               d_df['End of century'].values)[0])
                for indexi,i in enumerate(idx):
                    ax[i_season].scatter(6, displayed_range[1]- 2 - (indexi*2),
                                        facecolors='none', marker="D",
                                        edgecolors=c_points[i], s=160)
                    ax[i_season].text(6, displayed_range[1]- 2 - (indexi*2),
                                      str(int(d_df['End of century'].values[i])),
                                      verticalalignment='center',
                                      horizontalalignment='center',
                                      fontsize=7, weight='bold',color='black')
            return
                    

        shift_amount = 0.02
        # plt.yscale('log')
        ### Today
        df['Time Period'] = np.array(['Beginning of \n century']*len(df.index))
        df['Category_Num'] =  np.array([0]*len(df.index))

        sns.stripplot(data=df, x='Category_Num', y='Beginning of century',
                       hue='colors', log_scale=True,
                       ax=ax[i_season], jitter=False, palette=c_points,
                       legend=False, size=20)
        
        # daily data
        d_df['Category_Num_Shifted'] = df['Category_Num'] + shift_amount             
        # d_df['Time Period'] = np.array(['Beginning of \n century']*len(d_df.index))
        sns.stripplot(data=d_df, x='Category_Num_Shifted', y='Beginning of century',
                       hue='colors', ax=ax[i_season], jitter=False, marker="D",
                       palette=c_points, legend=False, size=19,alpha=1,
                       log_scale=True)
        ### mock entries
        for i in range(4):
            d_df['Category_Num_Shifted'] = df['Category_Num'] + (shift_amount*2)
            sns.stripplot(data=d_df, x='Category_Num_Shifted', y='Beginning of century',
                        hue='colors', ax=ax[i_season], jitter=False,
                        palette=c_points, legend=False, size=0, log_scale=True)


        ### Mid of century
        # hourly data
        df['Time Period'] = np.array(['Mid of \n century']*len(df.index))
        df['Category_Num'] =  np.array([1]*len(df.index))

        sns.stripplot(data=df, x='Category_Num', y='Mid of century',
                      hue='colors', ax=ax[i_season], jitter=False,
                      palette=c_points,legend=False, size=10, log_scale=True)
        # daily data
        # d_df['Time Period'] = np.array(['Mid of \n century']*len(d_df.index))
        d_df['Category_Num_Shifted'] = df['Category_Num'] + shift_amount     
        sns.stripplot(data=d_df, x='Category_Num_Shifted', y='Mid of century', 
                      hue='colors', ax=ax[i_season], jitter=False, marker="D",
                      palette=c_points, legend=False, size=9, alpha=1,
                      log_scale=True)

        # check_in_display_range(1)

        ### mock entries
        for i in range(3):
            d_df['Category_Num_Shifted'] = df['Category_Num'] + (shift_amount*2)
            sns.stripplot(data=d_df, x='Category_Num_Shifted', y='Beginning of century',
                       hue='colors', ax=ax[i_season], jitter=False,
                       palette=c_points, legend=False, size=0, log_scale=True)


        ### End of century
        # hourly data
        df['Time Period'] = np.array(['End of \n century']*len(df.index))
        df['Category_Num'] =  np.array([2]*len(df.index))

        sns.stripplot(data=df, x='Category_Num', y='End of century',
                      hue='colors', ax=ax[i_season], jitter=False,
                      palette=c_points,legend=False, size=10, log_scale=True)
        
        # daily data
        # d_df['Time Period'] = np.array(['End of \n century']*len(d_df.index))
        d_df['Category_Num_Shifted'] = df['Category_Num'] + shift_amount 
        sns.stripplot(data=d_df, x='Category_Num_Shifted', y='End of century',
                      hue='colors', ax=ax[i_season], jitter=False, marker="D",
                      palette=c_points,legend=False, size=9, alpha=.9,
                      log_scale=True)
        
        # check_in_display_range(2)
        
        ### ---------------------------------------------   
        ## Set lines between points
        for idx_c, idx in enumerate(df.index):
            idx = int(idx)
            ax[i_season].plot([0,3,6],
                              [df['Beginning of century'][idx],
                               df['Mid of century'][idx],
                               df['End of century'][idx]],
                              c=c_points[idx_c], linestyle='--',linewidth=0.8)
            ax[i_season].plot([1,4,7],
                              [d_df['Beginning of century'][idx],
                               d_df['Mid of century'][idx],
                               d_df['End of century'][idx]],
                              c=c_points[idx_c], linestyle=(0,(5, 10)),
                              linewidth=0.8,
                              alpha=.8)


        ax[i_season].set_title(f'{season_names[season]}', fontsize=18)
        if i_season != 0:
            ax[i_season].set_ylabel(None)
        else:
            ax[i_season].set_ylabel('Return Period', fontsize=16)
        ax[i_season].set_xlabel(None)
        ax[i_season].set_ylim(2,200)
        # ax[i_season].set_yscale('log')
        # ax[i_season].set_yticks(np.arange(displayed_range[0],
        #                                   displayed_range[1]+1,10),
        #                                   minor=True)
        ax[i_season].set_yticks([2,5,30,40,60,70,80,90,125,175], minor=True)
        ax[i_season].set_yticklabels([2,5,30,40,60,70,80,90,125,175], fontsize=8, minor=True)
        ax[i_season].grid(which='minor',alpha=0.9)

        ax[i_season].set_yticks([10,20,50,100,150,200], fontsize=14)
        ax[i_season].set_yticklabels([10,20,50,100,150,200], fontsize=14)
        
        # ax[i_season].set_xticklabels(['','Beginning of \n century','','',
        #                               'Mid of \n century','','',
        #                                 'End of \n century'
        #                                 ],fontsize=10)
        ### Set labels manually
        ax[i_season].set_xticklabels([])
        ax[i_season].text(x=0.15,y=-0.04, s='Beginning of \n century',
                          transform=ax[i_season].transAxes, fontsize=11,
                          verticalalignment='center',
                          horizontalalignment='center')
        ax[i_season].text(x=0.5,y=-0.04, s='Mid of \n century',
                          transform=ax[i_season].transAxes, fontsize=11,
                          verticalalignment='center',
                          horizontalalignment='center')
        ax[i_season].text(x=0.89,y=-0.04, s='End of \n century',
                          transform=ax[i_season].transAxes, fontsize=11,
                          verticalalignment='center',
                          horizontalalignment='center')
        
        

        ### Add Values of return period to the plot
        for y_i, y_value in enumerate(rp_df):
            ax[i_season].text(x=0, y=y_value,
                               fontsize=7, color='white',
                               s="{:.1f}".format(now_value[y_i]), weight='bold',
                               verticalalignment='center',
                               horizontalalignment='center')
            ax[i_season].text(x=1+shift_amount, y=y_value,
                               fontsize=7, color='white',
                               s="{:.1f}".format(d_now_value[y_i]), weight='bold',
                               verticalalignment='center',
                               horizontalalignment='center')
        

        ### Adding 'error bars' i.e. spread of individual models to the plot
        erbar_moc, erbar_eoc = [], []
        d_erbar_moc, d_erbar_eoc = [], []
        for errp in range(len(rp_of_interest)):
            erbar_moc.append([])
            erbar_eoc.append([])
            d_erbar_moc.append([])
            d_erbar_eoc.append([])

        for cpm in Institutes:
            ### hourly data
            fname_hist = (path_hist + 
                    f'/OneYearStep_{cpm}_{time_res}_RP_prmax_{season}.nc')
            fname_moc = (path_moc + 
                        f'/OneYearStep_{cpm}_{time_res}_RP_prmax_{season}.nc')
            fname_eoc = (path_eoc + 
                        f'/OneYearStep_{cpm}_{time_res}_RP_prmax_{season}.nc')
            
            ds_hist = xr.open_dataset(fname_hist)
            ds_moc = xr.open_dataset(fname_moc)
            ds_eoc = xr.open_dataset(fname_eoc)

            similar_moc, similar_eoc, hist_list = similar_value(ds_hist,ds_moc,ds_eoc)

            ds_hist.close()
            ds_moc.close()
            ds_eoc.close()

            ### daily data
            if cpm != 'CLMcom-ETH':
                fname_hist = (d_path_hist + 
                            f'/OneYearStep_{cpm}_1d_RP_prmax_{season}.nc')
                fname_moc = (d_path_moc + 
                            f'/OneYearStep_{cpm}_1d_RP_prmax_{season}.nc')
                fname_eoc = (d_path_eoc + 
                            f'/OneYearStep_{cpm}_1d_RP_prmax_{season}.nc')
                
                ds_hist = xr.open_dataset(fname_hist)
                ds_moc = xr.open_dataset(fname_moc)
                ds_eoc = xr.open_dataset(fname_eoc)

                d_similar_moc, d_similar_eoc, d_hist_list = \
                      similar_value(ds_hist,ds_moc,ds_eoc)

                ds_hist.close()
                ds_moc.close()
                ds_eoc.close()

                d_rp_df = np.array([])
                d_rp_df_moc = np.array([])
                d_rp_df_eoc = np.array([])


            rp_df = np.array([])
            rp_df_moc = np.array([])
            rp_df_eoc = np.array([])

            for rpoi in rp_of_interest:
                rpidx = return_periods.tolist().index(rpoi)
                #hourly data
                rp_df = np.append(rp_df, return_periods[rpidx])
                rp_df_moc = np.append(rp_df_moc, similar_moc[rpidx])
                rp_df_eoc = np.append(rp_df_eoc, similar_eoc[rpidx])
                #daily data
                if cpm != 'CLMcom-ETH':
                    d_rp_df = np.append(d_rp_df, return_periods[rpidx])
                    d_rp_df_moc = np.append(d_rp_df_moc, d_similar_moc[rpidx])
                    d_rp_df_eoc = np.append(d_rp_df_eoc, d_similar_eoc[rpidx])
                
           

            for rr in range(len(rp_of_interest)):
                erbar_moc[rr].append(rp_df_moc[rr])
                erbar_eoc[rr].append(rp_df_eoc[rr])
                if cpm != 'CLMcom-ETH':
                    d_erbar_moc[rr].append(d_rp_df_moc[rr])
                    d_erbar_eoc[rr].append(d_rp_df_eoc[rr])
        
        df_moc = pd.DataFrame(list(zip(erbar_moc)),
                           index=rp_df, 
                           columns=[title_name['rcp85_moc']])
        df_eoc = pd.DataFrame(list(zip(erbar_eoc)),
                           index=rp_df, 
                           columns=[title_name['rcp85_eoc']])

        ### Plot 'error bar' i.e. individual CPMs
        for idx_c in range(len(erbar_moc)):
            # ax[i_season].plot(['Mid of \n century']*len(erbar_moc[-idx_c]),
            #                   erbar_moc[-idx_c],c=c_points[-idx_c],
            #                   linestyle='-',linewidth=0.8)
            # ax[i_season].plot(['End of \n century']*len(erbar_eoc[-idx_c]),
            #                   erbar_eoc[-idx_c],c=c_points[-idx_c],
            #                   linestyle='-',linewidth=0.8)
            ### hourly data
            ax[i_season].scatter([3]*len(erbar_moc[-idx_c]),
                              erbar_moc[-idx_c],c=c_points[-idx_c],
                              alpha=.6, s=8)
            ax[i_season].scatter([6]*len(erbar_eoc[-idx_c]),
                              erbar_eoc[-idx_c],c=c_points[-idx_c],
                              alpha=.6, s=8)
            ### daily data
            ax[i_season].scatter([4]*len(d_erbar_moc[-idx_c]), 
                              d_erbar_moc[-idx_c],c=c_points[-idx_c],
                              alpha=.6, s=8,marker="D")
            ax[i_season].scatter([7]*len(d_erbar_eoc[-idx_c]), 
                              d_erbar_eoc[-idx_c],c=c_points[-idx_c],
                              alpha=.6, s=8,marker="D",)
            
        # add legend
        if legendset == False:
            legendset = True
            legend_elements = [
                    # Patch(facecolor='black', edgecolor='r',
                    #      label='Daily precipitation')
                    Line2D([0], [0], marker='o', color='black',
                            label='Hourly precipitation',
                           markersize=15,markerfacecolor='black'),
                    Line2D([0], [0], marker='D', color='black',
                            label='Daily precipitation',
                           markersize=15)]
            ax[1].legend(handles=legend_elements, bbox_to_anchor=(1.92, -0.08),
                         ncols=2, frameon=False, fontsize=14)
            # fig.legend(handles=legend_elements, bbox_to_anchor=(0.5, 0.05),
            #              ncols=2, frameon=False, fontsize=14, loc='center')
    

            
        # # dfmoc_transposed = df_moc.T
        # # dfeoc_transposed = df_eoc.T
        # # dfeoc_transposed['EndPeriod'] = np.array(['End of \n century']*len(dfeoc_transposed.index))
        # # dfmoc_transposed['MidPeriod'] = np.array(['Mid of \n century']*len(dfmoc_transposed.index))

        # # sns.pointplot(data=erbar_moc, 
        # #             #   errorbar=lambda erbar_moc: (erbar_moc.min(), erbar_moc.max()), 
        # #               errorbar='sd', 
        # #               capsize=.3, ax=ax[i_season])
        # # sns.pointplot(data=dfeoc_transposed, 
        # #               x='EndPeriod',
        # #               y=10,
        # #             #   errorbar=lambda erbar_moc: (erbar_moc.min(), erbar_moc.max()), 
        # #               errorbar='sd', 
        # #               capsize=.3, ax=ax[i_season])
        # plt.show()
        # breakpoint()
    
        # ax[i_season].set_xticks(fontsize=25)
        # ax[i_season].set_yticks(fontsize=25)
        # if season == 'JJA':
        #     breakpoint()



     
    plt.suptitle((f'Change of return period of max. precip. | ' 
                      +f'{region} '), y=0.98, fontsize=20)
   
    plt.savefig(os.path.join(plot_path, 
                # f'Lines_RP_change_{region}.pdf'),format='pdf',bbox_inches='tight')
                f'RP_change_{region}.pdf'),format='pdf',bbox_inches='tight')
    # plt.show()
    # breakpoint()
    

