#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
October 2023

Authors:
- Leandro Gimmi

Description:
Plot GEV values for different seasons and scenarios and models

"""
import os
import numpy as np
import xarray as xr
import pandas as pd
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import cartopy.feature as cfeature
from matplotlib.ticker import ScalarFormatter
from matplotlib.colors import LinearSegmentedColormap
# from tqdm import tqdm
import matplotlib.patches as mpatches

'''-------------------------------------------------------------------------'''

### Choose between rcp85_moc / rcp85_eoc 
scenario = 'rcp85_eoc'

### Choose between 1hr, 1d
time_res = '1hr'

### Seasons: DJF, MAM, JJA, SON
season = 'JJA'

### Ban et al. method to calculate CC signal or traditional
### Ban | traditional
CCmethod = 'Ban'

### CH2018 regions: CHNE, CHW, CHS ,CHAE or CHAW
## region = 'CHNE'

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)
return_periods = np.insert(return_periods,0,2)
# return_periods = np.append(return_periods, [150, 200, 250, 300])

'''-------------------------------------------------------------------------'''
plot_path = '/users/lgimmi/MeteoSwiss/figures/GEV_individual/CC/'
if time_res == '1d':
    plot_path = '/users/lgimmi/MeteoSwiss/figures/day/CC/'
path_region = '/scratch/snx3000/lgimmi/store/ALP-3/masks'

### ------------------------------- CPM ----------------------------------- ###
path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}')
# path_save_GEV = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
#              +f'{scenario}')
path_hist = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'historical')

Institutes = ['obs','CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH','CLMcom-JLU',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom','MOHC','ICTP']
CPMs = ['obs','CCLM5-0-14','CCLM5-0-9','COSMO-crCLIM','CCLM5-0-15','CCLM5-0-15',
        'AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME','HadREM3-RA-UM10.1',
        'RegCM4-7']

### ------------------------------- RCM ----------------------------------- ###
RCM_path_hist = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
                +f'historical')
RCM_path_data = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
             +f'{scenario}')
RCM_Institutes = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
RCMs = ['COSMO-crCLIM','ALADIN63','RACMO23E','HCLIM38-ALADIN',
            'RegCM4-7']

'''-------------------------------------------------------------------------'''
region_numbers = {"CH":1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1, "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                 "SON": 'Autumn'}

if scenario == 'rcp85_moc':
    path_data = path_data[:-4] + '/moc'
    RCM_path_data = RCM_path_data[:-4] + '/moc'
elif scenario == 'rcp85_eoc':
    path_data = path_data[:-4] + '/eoc'
    RCM_path_data = RCM_path_data[:-4] + '/eoc'

for iterate, region in enumerate(region_numbers.keys()):
    print(f'{iterate+1}/{len(region_numbers.keys())}')
    fig = plt.figure(figsize=(15, 10))
    gs = fig.add_gridspec(nrows=3, ncols=4)
    row = 0
    col = 0
    if season == 'DJF':
        max_y = 100
    elif season == 'JJA':
        max_y = 100
    elif season == 'MAM':
        max_y = 100
    elif season == 'SON':
        max_y = 100

    maxima_list = []
    r_maxima_list = []
    for i_cpm, cpm in enumerate(Institutes):
        if cpm == 'obs':
            continue

        ax = fig.add_subplot(gs[row,col])

        ### Niklina Paper calculation of CC
        filename = (path_data + '/' + f'CC_{cpm}_{region}_{season}.npy')
        data = np.load(filename, allow_pickle=True)
        ### Standard way of doin it
        filename_hist = (path_hist + f'/Values_{cpm}_{region}_{season}.npy')
        data_hist = np.load(filename_hist, allow_pickle=True)
        filename_rcp = (path_data + f'/Values_{cpm}_{region}_{season}.npy')
        data_rcp = np.load(filename_rcp, allow_pickle=True)

        ten_perc, ninety_perc, median, mean, local = [], [], [], [], []
        h_ten_perc, h_ninety_perc, h_median, h_mean = [], [], [], []
        cc_median, cc_90_list, cc_10_list = [], [], []
        
        for rp in return_periods:
            ### Regional mean
            ### Standard way of doin CC-calculus
            cc_median.append(np.nanmean(((
                np.array(data_rcp[rp][1]) - np.array(data_hist[rp][1])) / 
                np.array(data_hist[rp][1])) * 100 ))
            # cc_median.append(((
            #     np.nanmean(data_rcp[rp][1]) - np.nanmean(data_hist[rp][1])) / 
            #     np.nanmean(data_hist[rp][1])) * 100 )
            
            cc_90_list.append(np.nanmean(((
                np.array(data_rcp[rp][3]) - np.array(data_hist[rp][3])) / 
                np.array(data_hist[rp][3])) * 100))
            cc_10_list.append(np.nanmean(((
                np.array(data_rcp[rp][2]) - np.array(data_hist[rp][2])) / 
                np.array(data_hist[rp][2])) * 100))
            
            ### Niklina Paper calculation of CC
            ten_perc.append(np.nanmean(data[rp][2]))
            ninety_perc.append(np.nanmean(data[rp][3]))
            mean.append(np.nanmean(data[rp][0]))
            median.append(np.nanmean(data[rp][1]))

            if rp == return_periods[-1]:
                maxima_list.append(np.nanmean(data[rp][3]))
        
        for i in [ten_perc,ninety_perc,mean,median]:
            local.append(i)

         ### Add conventional regional climate models
        if cpm in RCM_Institutes:
            ### Niklina Paper calculation of CC
            r_fname = (RCM_path_data + f'/CC_{cpm}_{region}_{season}.npy')
            r_data = np.load(r_fname, allow_pickle=True)
            ### Standard way of doin CC-calculus
            r_filename_hist = (RCM_path_hist + 
                               f'/Values_{cpm}_{region}_{season}.npy')
            r_data_hist = np.load(r_filename_hist, allow_pickle=True)
            r_filename_rcp = (RCM_path_data + 
                               f'/Values_{cpm}_{region}_{season}.npy')
            r_data_rcp = np.load(r_filename_rcp, allow_pickle=True)

            r_ten_perc, r_ninety_perc, r_median, r_mean = [], [], [], []
            rh_ten_perc, rh_ninety_perc, rh_median, rh_mean = [], [], [], []
            r_cc_median, r_cc_90_list, r_cc_10_list = [], [], []
            
            for rp in return_periods:
                ### Standard way of doin CC-calculus
                r_cc_median.append(np.nanmean(((
                np.array(r_data_rcp[rp][1]) - np.array(r_data_hist[rp][1])) / 
                np.array(r_data_hist[rp][1])) * 100 ))

                # r_cc_median.append(((
                # np.nanmean(r_data_rcp[rp][1]) - np.nanmean(r_data_hist[rp][1])) / 
                # np.nanmean(r_data_hist[rp][1])) * 100 )

                r_cc_90_list.append(np.nanmean(((
                np.array(r_data_rcp[rp][3]) - np.array(r_data_hist[rp][3])) / 
                np.array(r_data_hist[rp][3])) * 100))

                r_cc_10_list.append(np.nanmean(((
                np.array(r_data_rcp[rp][2]) - np.array(r_data_hist[rp][2])) / 
                np.array(r_data_hist[rp][2])) * 100))

                ### Niklina Paper calculation of CC
                r_ten_perc.append(np.nanmean(r_data[rp][2]))
                r_ninety_perc.append(np.nanmean(r_data[rp][3]))
                r_mean.append(np.nanmean(r_data[rp][0]))
                r_median.append(np.nanmean(r_data[rp][1]))
                if rp == return_periods[-1]:
                    r_maxima_list.append(np.nanmean(r_data[rp][3]))
            if CCmethod == 'Ban':
                ### Niklina Paper calculation of CC   
                ax.plot(return_periods, r_median, label='median (parent RCM)',
                            color='crimson', linestyle='-', zorder=-10)
                ax.fill_between(return_periods,r_ten_perc,r_ninety_perc,
                    label = r'10$^{th}$-90$^{th}$P '+ 
                    '(parent RCM)', color='lightcoral', alpha=0.2, zorder=-20)
            elif CCmethod == 'traditional':
                ### Standard way of doin CC-calculus
                ax.plot(return_periods, r_cc_median, label='(2)median (parent RCM)',
                            color='crimson', linestyle='-', zorder=-10)
                ax.fill_between(return_periods,r_cc_10_list,r_cc_90_list,
                    label = r'(2)10$^{th}$-90$^{th}$P '+ 
                    '(parent RCM)', color='lightcoral', alpha=0.2, zorder=-20)
        # try:
        #     rm_old_file = ('rm ' + '/users/lgimmi/MeteoSwiss/local' + 
        #         f'/local_{cpm}_{region}_{season}.npy')
        #     os.system(rm_old_file)
        # except:
        #     print('No old file to remove')

        # np.save('/users/lgimmi/MeteoSwiss/local/' +
        #         f'local_{cpm}_{region}_{season}.npy',
        #         np.array(local, dtype=object), allow_pickle=True)

        # cc_signal = (np.array(median)-np.array(h_median))/np.array(median)*100
        # cc_90 = ( np.array(ninety_perc)-np.array(h_ninety_perc))/np.array(ninety_perc)*100
        # cc_10 =  (np.array(ten_perc)-np.array(h_ten_perc))/np.array(ten_perc)*100

        ax.plot(return_periods, np.zeros(len(return_periods)), color='k',
                 linestyle='-',linewidth=0.8,alpha=0.6)
        if CCmethod == 'Ban':
            ### Niklina Paper calculation of CC 
            ax.plot(return_periods, median, label='median', color='dodgerblue',
                    linestyle='-')
            ax.fill_between(return_periods,ten_perc,ninety_perc,
                            label = r'10$^{th}$-90$^{th}$P (CPM)', color='lightblue',
                            alpha=0.5,zorder=-15)
        elif CCmethod == 'traditional':
            ### Standard way of doin CC-calculus
            ax.plot(return_periods, cc_median, label='(2)median', color='dodgerblue',
                        linestyle='-')
            ax.fill_between(return_periods,cc_10_list,cc_90_list,
                            label = r'(2) 10$^{th}$-90$^{th}$P (CPM)', color='lightblue',
                            alpha=0.5,zorder=-15)
        


        ax.text(23, max_y+(1/15*max_y), f'{cpm}',fontsize=9,
                        #weight='bold',
                    verticalalignment='center', horizontalalignment='center',
                    rotation=0)

        major_ticks_y = np.arange(-max_y, max_y+1, 2*max_y/10*2)
        minor_ticks_y = np.arange(-max_y, max_y+1, 2*max_y/10)
        ax.set_yticks(major_ticks_y)
        ax.set_yticks(minor_ticks_y, minor=True)

        ax.grid(which='both', axis='y', alpha=0.3)
        if row == 2:
            ax.set_xlabel('Returnperiod [yr]')
        elif row == 1 and col in [3]:
            ax.set_xlabel('Returnperiod [yr]')
        if col == 0:
            ax.set_ylabel('Precipitation change [%]')
        
        ax.set_xscale('log')
        ax.set_xticks([5,10,20,50,100,300])
        ax.get_xaxis().set_major_formatter(ScalarFormatter())

        ax.set_ylim(-max_y,max_y)
        plt.xlim(return_periods[0],return_periods[-1])
        if col == 3:
            col = 0
            row += 1
        else:
            col += 1
        

    handles, labels = plt.gca().get_legend_handles_labels()
    order = [2,3,0,1]
    # fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
    #            bbox_to_anchor=(0.67, 0.19), frameon=False, fontsize=12)
    fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
            bbox_to_anchor=(0.877, 0.24), frameon=False, fontsize=12)
    # plt.suptitle(f'{season}, {region}', y=0.93, fontsize=15)
    plt.suptitle(f'Change Signal | {season_names[season]} | {region} | {time_res}',
                      y=0.93, fontsize=15)

    ### --------------------------------------------------------------------###
    ### Add overview of region and stations to plot
    ### --------------------------------------------------------------------###

    ax = fig.add_subplot(gs[row,col], projection=ccrs.PlateCarree())
    map_ext = np.array([5.2, 11.4, 45.0, 48.3])  # [degree]
    ax.coastlines(color='grey', linewidth=0.5)
    ax.add_feature(cfeature.BORDERS, edgecolor= 'black',
        linewidth = 0.5 )
    ax.set_extent(map_ext, crs=ccrs.PlateCarree())
    ax.set_aspect("auto")

    ### open region and station data
    path_stations = '/users/lgimmi/MeteoSwiss/data' 
    path_region = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
    filename_stations = (path_stations + '/station_metadat_rre150h0_1995_2010_'
                        +'allstats_andpartnerstats_withNA.csv')
    filename_regions = (path_region + '/station_region_latlon.nc')

    if region != 'CH':
        filename_regions = (path_region + '/station_region_latlon.nc')
    else:
        filename_regions = (path_region + '/station_region_latlon_maskCH.nc')

    regions = xr.open_dataset(filename_regions)

    if region != 'CH':
        ch = regions.orog
    else:
        ch = regions['mask_CH']

    data = pd.read_csv(filename_stations)
    lon = data['longitude']
    lat = data['latitude']

    colors = {"CH":"#da291c","CHAE": "darkkhaki", "CHAW": "darkgoldenrod",
            "CHW": "darkgreen", "CHNE": "green", "CHS": "gold"}

    cmap = LinearSegmentedColormap.from_list('custom_colormap', [colors[region],
                                            (1, 1, 1)] , N=2 )
    masked_region = ch.where(ch == region_numbers[region], drop=True) 

    if region == 'CH': 
        col = ax.contourf(masked_region.lon, masked_region.lat, masked_region,
                    cmap=cmap, alpha=1.0)
        # ax.plot(masked_region.lon[114:115],masked_region.lat[53:54],marker='P',
        #         color='white', transform=ccrs.PlateCarree(),markersize=30)
    else:
        col = ax.contourf(masked_region.lon, masked_region.lat, masked_region,
                    cmap=cmap, alpha=0.8)

    ### --------------------------------------------------------------------###
    plt.show()
    breakpoint()
    
    if CCmethod == 'Ban':
        plt.savefig(os.path.join(plot_path, 
                    f'BAN_cc_{scenario[-3:]}_GEV_{season}_{region}.pdf'),
                    format='pdf',bbox_inches='tight')
    elif CCmethod == 'traditional':
        plt.savefig(os.path.join(plot_path, 
                    f'cc_{scenario[-3:]}_GEV_{season}_{region}.pdf'),
                    format='pdf',bbox_inches='tight')
    plt.close(fig)
    
