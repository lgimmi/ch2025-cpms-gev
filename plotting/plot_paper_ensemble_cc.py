#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
January 2024

Authors:
- Leandro Gimmi

Description:
Plot GEV values for different seasons and scenarios and models

"""
import os
import copy
import string
import numpy as np
import xarray as xr
import pandas as pd
import matplotlib as mpl
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import cartopy.feature as cfeature
from matplotlib.ticker import ScalarFormatter
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.patches as mpatches
import warnings
warnings.simplefilter("ignore")


'''-------------------------------------------------------------------------'''

### Choose between rcp85_moc, rcp85_eoc
scenario = 'rcp85_eoc'

### xHourly, xDaily
time_res = ['1d','1hr']

### Seasons: DJF, MAM, JJA, SON
# seasons = ['JJA', 'DJF']
seasons = ['MAM', 'SON']
### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)
# rp_index = np.arange(2,201,1)

'''-------------------------------------------------------------------------'''
### ------------------------------- CPMs ---------------------------------- ###
plot_path = f'/users/lgimmi/MeteoSwiss/figures/paper/cc/'

path_region = '/scratch/snx3000/lgimmi/store/ALP-3/masks'

Institutes = ['CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH',
              'CLMcom-JLU', 'CLMcom-KIT','CNRM','KNMI','HCLIMcom', 'ensemble']

### ------------------------------- RCMs ----------------------------------- ###

RCM_path_region = '/scratch/snx3000/lgimmi/store/rcm/masks'

RCM_Institutes = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom','ensemble']


region_numbers = {"CH":1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1,
                   "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                 "SON": 'Autumn'}

alphabet = list(string.ascii_lowercase)

'''-------------------------------------------------------------------------'''

for region in region_numbers.keys():
    ### Open Figure
    # fig = plt.figure(figsize=(10, 9))
    fig = plt.figure(figsize=(6, 5.4))
    gs = fig.add_gridspec(nrows=2, ncols=2)
    row, column, bet = 0, 0, 0
    print((f'Region: {region}').center(60,'~'))
    for season in seasons:
        print((f'Seasons: {season}').center(1,' '))
        for tres in time_res:
            ax = fig.add_subplot(gs[row,column])
            if tres in ['1hr','1d']:
                divide = 1
            # elif tres in ['3hr','3d']:
            #     divide = 3
            # elif tres in ['6hr']:
            #     divide = 6
            # elif tres in ['5d']:
            #     divide = 5
            print((f'Time resolution: {tres}').center(40,' '))
            ensemble_list = []
            RCM_ensemble_list = []
            for cpm in Institutes:       
                if cpm != 'ensemble':      
                    path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{tres}/'
                                +f'pr_GEV/{scenario}')
                    RCM_path_data = (f'/scratch/snx3000/lgimmi/store/rcm/{tres}/'
                                +f'pr_GEV/{scenario}')
                    
                    if scenario == 'rcp85_moc':
                        path_data = path_data[:-4] + '/moc'
                        RCM_path_data = RCM_path_data[:-4] + '/moc'
                    elif scenario == 'rcp85_eoc':
                        path_data = path_data[:-4] + '/eoc'
                        RCM_path_data = RCM_path_data[:-4] + '/eoc'

                    filenmame = (path_data + 
                                 f'/{cpm}_cc_{tres}_RP_prmax_{season}.nc')
                    ds = xr.open_dataset(filenmame)

                    ds_region = ds[region]
                    rp_index = ds_region.returnperiod.values

                    if cpm in RCM_Institutes:
                        RCM_filenmame = (RCM_path_data + 
                                    f'/{cpm}_cc_{tres}_RP_prmax_{season}.nc')
                        RCM_ds = xr.open_dataset(RCM_filenmame)
                        RCM_ds_region = RCM_ds[region]
                        RCM_rp_index = RCM_ds_region.returnperiod.values
                    
                    lmean, lmedian, ltenP, lninetyP = [], [], [], []
                    RCM_lmedian = []
                    for rp in return_periods:
                        rpidx = np.where(rp_index == rp)[0][0]
                        vmean = ds_region[rpidx][0]
                        vmedian = ds_region[rpidx][1]
                        vtenP = ds_region[rpidx][2]
                        vninetyP = ds_region[rpidx][3]

                        ### Use divide to get hourly values
                        lmean.append(np.nanmean(vmean) / divide)
                        lmedian.append(np.nanmean(vmedian) / divide)
                        ltenP.append(np.nanmean(vtenP) / divide)
                        lninetyP.append(np.nanmean(vninetyP) / divide)    

                        if cpm in RCM_Institutes:
                            RCM_rpidx = np.where(RCM_rp_index == rp)[0][0]
                            RCM_vmedian = RCM_ds_region[RCM_rpidx][1]    
                            RCM_lmedian.append(np.nanmean(RCM_vmedian) / divide)

                    ### Plot
                    # if cpm == 'EnsembleMean':
                    #     ax.plot(return_periods, lmedian, label='Ensemble Mean',
                    #         color='dodgerblue', linestyle='-')
                    #     ones = np.zeros(len(return_periods))
                    #     ax.plot(return_periods, ones, color='black',
                    #             linewidth=0.8)
                    #     ax.plot([-10,0], [0,0], label=f'Individual CPMs',
                    #         color='dodgerblue', linestyle='-', alpha=0.5,
                    #         linewidth=1, zorder=-100)
                    # else:
                    #     ax.plot(return_periods, lmedian, label=f'Individual CPMs',
                    #         color='dodgerblue', linestyle='-', alpha=0.3,
                    #         linewidth=0.5)
                        
                    ax.plot(return_periods, lmedian, label=f'Individual CPMs',
                            color='dodgerblue', linestyle='-', alpha=0.5,
                            linewidth=0.5)
                    ensemble_list.append(lmedian)
                    if cpm in RCM_Institutes:
                        RCM_ensemble_list.append(RCM_lmedian)
                        ax.plot(return_periods, RCM_lmedian,
                                 label=f'Individual RCMs', color='crimson',
                                 linestyle='-', alpha=0.5, linewidth=0.5)

                else:
                    # ensemble = np.median(np.array(ensemble_list),axis=0)
                    ensemble = np.mean(np.array(ensemble_list),axis=0)
                    if season == 'DJF' and tres == '1d':
                        ensemble = np.mean(np.array(ensemble_list[1:]),axis=0)
                    RCM_ensemble = np.mean(np.array(RCM_ensemble_list),axis=0)
                    ax.plot(return_periods, ensemble, 
                            label='Ensemble Mean CPMs', color='royalblue',
                            linestyle='-')
                    ax.plot(return_periods, RCM_ensemble,
                             label='Ensemble Mean RCMs', color='crimson',
                             linestyle='-')
                    
                    ones = np.zeros(len(return_periods))
                    ax.plot(return_periods, ones, color='black',
                            linewidth=0.8)
                    
                    ax.plot([-10,0], [0,0], label=f'Individual CPMs',
                        color='dodgerblue', linestyle='-', alpha=0.7,
                        linewidth=1, zorder=-100)
                    ax.plot([-10,0], [0,0], label=f'Individual RCMs',
                        color='crimson', linestyle='-', alpha=0.7,
                        linewidth=1, zorder=-100)
           
            ### Plot settings
            max_y = 60
            min_y = -20
            # ax.set_facecolor('whitesmoke')
            # if row == 0:
            ax.set_title(f'{season_names[season]} | {tres}',fontsize=11,
                              loc='left')
            ax.set_title(f'{alphabet[bet]}',fontsize=10,
                              loc='right',x=0.98 )
            # if column == 0:
            #     props = dict(facecolor='white', alpha=0.5)
            #     ax.text(-0.27,0.35,f'{season_names[season]}',rotation=90,
            #         transform=ax.transAxes, fontsize=15)

            major_ticks_y = np.arange(-max_y, max_y+1, max_y/10*2)
            minor_ticks_y = np.arange(-max_y, max_y+1, max_y/10*1)
            ax.set_ylabel('Change in Intensity [%]', fontsize=9,
                          labelpad=0.2)

            ax.set_yticks(major_ticks_y)
            ax.set_yticks(minor_ticks_y, minor=True)
            ax.tick_params(axis='both', which='both', labelsize=8)
            ax.set_ylim(min_y,max_y)
            ax.grid(which='both', axis='y', alpha=0.3)

            ax.set_xscale('log')
            # if row == 1:
            ax.set_xlabel('Returnperiod [yr]', fontsize=9)
                
            ax.set_xticks([5,10,20,50,100], fontsize=7)
            ax.get_xaxis().set_major_formatter(ScalarFormatter())
            plt.xlim(return_periods[0],return_periods[-1])
            plt.suptitle(f' Projected Change | RCP8.5 | Region: {region}' + '\n'
                          + 'Historical (1995-2005) vs. End of Century '
                          +'(2090-2099)', y=1.02,
                          fontsize=12)
            handles, labels = plt.gca().get_legend_handles_labels()
            order = [-4,-3,-2,-1]
            fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
                      frameon=False, fontsize=8,  ncol=2,
                      bbox_to_anchor=(0.75, 0.037)) #loc='lower center',

            if column == 1:
                column = 0
                row += 1
            else:
                column += 1
            bet +=  1

    # handles, labels = plt.gca().get_legend_handles_labels()
    # order = [-2,-1]

    # fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
    #         bbox_to_anchor=(0.5, 0.04), frameon=False, fontsize=10, ncols=2,
    #         loc='center')

    #  bbox_to_anchor=(0.73, 0.04)
    # plt.subplots_adjust( wspace=0.2, hspace=0.2)
    plt.subplots_adjust( wspace=0.35, hspace=0.45)
    plt.savefig(os.path.join(plot_path,
                    f'Paper_Ensemble_CC_{region}_{seasons[0]}{seasons[1]}.pdf'),
                    format='pdf',bbox_inches='tight')
    # plt.show()
    # breakpoint()
    # plt.savefig(os.path.join(plot_path, f'Overview_EnsembleMean_{region}.pdf'),
    #                     format='pdf',bbox_inches='tight')
    plt.close()


