#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
October 2023

Authors:
- Leandro Gimmi

Description:
Plot GEV values for different seasons and scenarios and models

"""
import os
import numpy as np
import xarray as xr
import pandas as pd
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import cartopy.feature as cfeature
from matplotlib.ticker import ScalarFormatter
from matplotlib.colors import LinearSegmentedColormap
# from tqdm import tqdm
import matplotlib.patches as mpatches


'''-------------------------------------------------------------------------'''

### Choose between evaluation / historical 
# scenario = 'historical'

### xHourly, xDaily
time_res = '1hr'

### Seasons: DJF, MAM, JJA, SON
season = 'JJA'

### CH2018 regions: CH, CHNE, CHW, CHS, CHAE or CHAW
## region = 'CHAW'

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)
# return_periods = np.insert(return_periods,0,2)
# return_periods = np.append(return_periods, [150, 200, 250, 300])

region_numbers = {"CH":1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1, "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                 "SON": 'Autumn'}
'''-------------------------------------------------------------------------'''
for iterate, region in enumerate(region_numbers.keys()):
    
    print(f'{iterate+1}/{len(region_numbers.keys())}')
    fig = plt.figure(figsize=(15, 10))
    gs = fig.add_gridspec(nrows=3, ncols=4)
    ax_number = 0
    
    if season == 'DJF':
        max_y = 30
    elif season == 'JJA':
        max_y = 100
    elif season == 'MAM':
        max_y = 100
    elif season == 'SON':
        max_y = 100

    for i_scen, scenario in enumerate([ 'historical', 'rcp85_eoc']):
        row = 0
        col = 0
        
        ### ------------------------------- CPM --------------------------- ###
        plot_path = f'/users/lgimmi/MeteoSwiss/figures/GEV_individual/hist_rcp/'
        path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
                    +f'{scenario}')
        path_save_GEV = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
                    +f'{scenario}')
        path_region = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
        path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'

        Institutes = ['obs','CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH','CLMcom-JLU',
                    'CLMcom-KIT','CNRM','KNMI','HCLIMcom','MOHC','ICTP']
        CPMs = ['obs','CCLM5-0-14','CCLM5-0-9','COSMO-crCLIM','CCLM5-0-15','CCLM5-0-15',
                'AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME','HadREM3-RA-UM10.1',
                'RegCM4-7']
        ### ------------------------------- RCM --------------------------- ###
        RCM_path_data = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
                    +f'{scenario}')
        path_region = '/scratch/snx3000/lgimmi/store/rcm/masks'
        path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
        if scenario == 'evaluation':
            RCM_Institutes = ['CLMcom-BTU','CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
            RCMs = ['CCLM5-0-9','COSMO-crCLIM','ALADIN62','RACMO23E','HCLIM38-ALADIN',
                    'RegCM4-7']
        else:
            RCM_Institutes = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
            RCMs = ['COSMO-crCLIM','ALADIN62','RACMO23E','HCLIM38-ALADIN',
                    'RegCM4-7']

        '''-------------------------------------------------------------------------'''

        if scenario == 'rcp85_moc':
            path_data = path_data[:-4] + '/moc'
            RCM_path_data = RCM_path_data[:-4] + '/moc'
        elif scenario == 'rcp85_eoc':
            path_data = path_data[:-4] + '/eoc'
            RCM_path_data = RCM_path_data[:-4] + '/eoc'

        if i_scen == 0:
            filename_station = (path_save_GEV + f'/Values_stations_{season}_'
                                +f'{scenario}.npy')
            filename_region = (path_region_stations +f'/stations_region_{scenario}.csv')
            station_data = np.load(filename_station, allow_pickle=True)
            station_region = pd.read_csv(filename_region)
            st_ten_perc, st_ninety_perc, st_median, st_mean, st_local = [],[],[],[], []

            for rp in return_periods:
                st1, st2, st3, st4 = [],[],[],[]
                for i_station, station in enumerate(station_region.keys()):
                    if station_region.values[0][i_station] != region and region != 'CH':
                        continue
                    st1.append(station_data[i_station][rp][2])
                    st2.append(station_data[i_station][rp][3])
                    st3.append(station_data[i_station][rp][0])
                    st4.append(station_data[i_station][rp][1])
                
                st_ten_perc.append(np.nanmean(st1))
                st_ninety_perc.append(np.nanmean(st2))
                st_mean.append(np.nanmean(st3))
                st_median.append(np.nanmean(st4))

        maxima_list = []
        r_maxima_list = []
        for i_cpm, cpm in enumerate(Institutes):
            if cpm == 'obs':
                continue

            if i_scen == 0:
                ax = fig.add_subplot(gs[row,col])
            else:
                ax = fig.axes[ax_number]
                ax_number += 1

            if scenario == 'rcp85_moc':
                path_data = path_data[:-4] + '/moc'
            elif scenario == 'rcp85_eoc':
                path_data = path_data[:-4] + '/eoc'

            filename = (path_data + '/' + f'Values_{cpm}_{region}_{season}.npy')
            data = np.load(filename, allow_pickle=True)

            ten_perc, ninety_perc, median, mean, local = [], [], [], [], []
            
            for rp in return_periods:
                ten_perc.append(np.nanmean(data[rp][2]))
                ninety_perc.append(np.nanmean(data[rp][3]))
                mean.append(np.nanmean(data[rp][0]))
                median.append(np.nanmean(data[rp][1]))
                if rp == return_periods[-1]:
                    maxima_list.append(np.nanmean(data[rp][3]))

            ### Add conventional regional climate models
            if cpm in RCM_Institutes:
                r_fname = (RCM_path_data + f'/Values_{cpm}_{region}_{season}.npy')
                r_data = np.load(r_fname, allow_pickle=True)

                r_ten_perc, r_ninety_perc, r_median, r_mean = [], [], [], []
                
                for rp in return_periods:
                    r_ten_perc.append(np.nanmean(r_data[rp][2]))
                    r_ninety_perc.append(np.nanmean(r_data[rp][3]))
                    r_mean.append(np.nanmean(r_data[rp][0]))
                    r_median.append(np.nanmean(r_data[rp][1]))
                    if rp == return_periods[-1]:
                        r_maxima_list.append(np.nanmean(r_data[rp][3]))
                if i_scen == 0:
                    ax.plot(return_periods, r_median, label='median Historical (parent RCM)',
                                color='crimson', linestyle='-', zorder=-10)
                    ax.fill_between(return_periods,r_ten_perc,r_ninety_perc,
                        label = r'10$^{th}$-90$^{th}$P Historical'+ #os.linesep +
                        '(parent RCM)', color='lightcoral', alpha=0.2, zorder=-55)
                else:
                    ax.plot(return_periods, r_median, label='median RCP8.5 (parent RCM)',
                                color='brown', linestyle='-', zorder=-10)
                    ax.plot(return_periods, r_median, linestyle=(0,(3,5,1,5)),
                            label = r'10$^{th}$-90$^{th}$P RCP8.5'+ #os.linesep +
                        '(parent RCM)', color='brown', alpha=0.8, zorder=-25)
                    ax.plot(return_periods, r_median, linestyle=(0,(3,5,1,5)),
                            label = r'10$^{th}$-90$^{th}$P RCP8.5'+ #os.linesep +
                        '(parent RCM)', color='brown', alpha=0.8, zorder=-25)

            
            if i_scen == 0:
                ### Add Station data
                ax.plot(return_periods, st_median, label='station data',
                        color='k', linestyle='--')
                ax.plot(return_periods, st_ten_perc,
                        label=r'10$^{th}$-90$^{th}$ percentile' + #os.linesep +
                        '(Stations)', color='k', linestyle='-',linewidth=1,
                        zorder=-5)
                ax.plot(return_periods, st_ninety_perc, color='k',
                        linestyle='-', linewidth=1,zorder=-5)
                 
                ### Add CPMs
                ax.plot(return_periods, median, label='median Historical (CPM)',
                        color='dodgerblue', linestyle='-')
                # ax.plot(return_periods, ten_perc, 
                #         label = r'10$^{th}$-90$^{th}$P Historical' 
                #                 +'(CPM)', color='dodgerblue',
                #                 alpha=1,zorder=-15,linestyle=(0,(3,5,1,5)))
                # ax.plot(return_periods, ninety_perc, 
                #         label = r'10$^{th}$-90$^{th}$P Historical' 
                #                 +'(CPM)', color='dodgerblue',
                #                 alpha=1,zorder=-15,linestyle=(0,(3,5,1,5)))

                ax.fill_between(return_periods,ten_perc,ninety_perc,
                                label = r'10$^{th}$-90$^{th}$P Historical' 
                                +'(CPM)', color='lightblue',
                                alpha=0.5,zorder=-50)
                ax.text(23, max_y+(1/25*max_y), f'{cpm}',fontsize=9,
                                #weight='bold',
                            verticalalignment='center', horizontalalignment='center',
                            rotation=0)
                
            else:
                ### Add CPMs rcp
                ax.plot(return_periods, median, label='median RCP8.5 (CPM)',
                        color='mediumblue', linestyle='-',zorder=-35)
                ax.plot(return_periods, ten_perc, 
                        label = r'10$^{th}$-90$^{th}$P RCP8.5' 
                        +'(CPM)', color='blue',linestyle=(0,(3,5,1,5)),
                        alpha=0.7,zorder=-35)
                ax.plot(return_periods, ninety_perc, 
                        label = r'10$^{th}$-90$^{th}$P RCP8.5' 
                        +'(CPM)', color='blue',linestyle=(0,(3,5,1,5)),
                        alpha=0.7,zorder=-35)
                # ax.fill_between(return_periods,ten_perc,ninety_perc,
                #                 label = r'10$^{th}$-90$^{th}$P RCP8.5' 
                #                 +'(CPM)', color='lightblue',
                #                 alpha=0.5,zorder=-15)


            major_ticks_y = np.arange(0, max_y+1, max_y/10*2)
            minor_ticks_y = np.arange(0, max_y+1, max_y/10)
            ax.set_yticks(major_ticks_y)
            ax.set_yticks(minor_ticks_y, minor=True)


            ax.grid(which='both', axis='y', alpha=0.3)
            if row == 2:
                ax.set_xlabel('Returnperiod [yr]')
            elif row == 1 and col in [3]:
                ax.set_xlabel('Returnperiod [yr]')
            if col == 0:
                ax.set_ylabel('Precipitation intensity [mm/h]')
            
            ax.set_xscale('log')
            ax.set_xticks([5,10,20,50,100])
            ax.get_xaxis().set_major_formatter(ScalarFormatter())

            ax.set_ylim(0,max_y)
            # ax.set_ylim(-max_y,max_y)

            plt.xlim(return_periods[0],return_periods[-1])
            if col == 3:
                col = 0
                row += 1
            else:
                col += 1
            # Mock entry
            ax.plot(np.nan,np.nan, 'ko',markersize=3, label = 'stations')

    handles, labels = plt.gca().get_legend_handles_labels()

    if cpm in RCM_Institutes:
        # order = [5,2,3,4,0,1]
        order = [6,2,3,4,5,0,1,10,11,7,8]
    # fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
    #            bbox_to_anchor=(0.67, 0.19), frameon=False, fontsize=12)
    fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
            bbox_to_anchor=(0.87, 0.307), frameon=False, fontsize=8)
 
    plt.suptitle(f'Historical vs RCP8.5 | {season_names[season]} | {region} | {time_res}',
                  y=0.93, fontsize=15)

    ### --------------------------------------------------------------------###
    ### Add overview of region and stations to plot
    ### --------------------------------------------------------------------###

    ax = fig.add_subplot(gs[row,col], projection=ccrs.PlateCarree())
    map_ext = np.array([5.2, 11.4, 45.0, 48.3])  # [degree]
    ax.coastlines(color='grey', linewidth=0.5)
    ax.add_feature(cfeature.BORDERS, edgecolor= 'black',
        linewidth = 0.5 )
    ax.set_extent(map_ext, crs=ccrs.PlateCarree())
    ax.set_aspect("auto")

    ### open region and station data
    path_stations = '/users/lgimmi/MeteoSwiss/data' 
    path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
    filename_stations = (path_stations + '/station_metadat_rre150h0_1995_2010_'
                        +'allstats_andpartnerstats_withNA.csv')
    if region != 'CH':
        filename_regions = (path_region_stations + '/station_region_latlon.nc')
    else:
        filename_regions = (path_region_stations + 
                            '/station_region_latlon_maskCH.nc')

    regions = xr.open_dataset(filename_regions)
    if region != 'CH':
        ch = regions.orog
    else:
        ch = regions['mask_CH']
    data = pd.read_csv(filename_stations)
    lon = data['longitude']
    lat = data['latitude']

    colors = {"CH":"#da291c","CHAE": "darkkhaki", "CHAW": "darkgoldenrod",
            "CHW": "darkgreen", "CHNE": "green", "CHS": "gold"}

    cmap = LinearSegmentedColormap.from_list('custom_colormap', [colors[region],
                                            (1, 1, 1)] , N=2 )
    masked_region = ch.where(ch == region_numbers[region], drop=True) 
    if region == 'CH': 
        col = ax.contourf(masked_region.lon, masked_region.lat, masked_region,
                    cmap=cmap, alpha=1.0)
        ax.plot(masked_region.lon[114:115],masked_region.lat[53:54],marker='P',
                color='white', transform=ccrs.PlateCarree(),markersize=30)
    else:
        col = ax.contourf(masked_region.lon, masked_region.lat, masked_region,
                    cmap=cmap, alpha=0.8)

    ### Check in which region (rounded) lat/lon of stations are found
    num_of_stations = 0
    for i, station in enumerate(data['nat_abbr']):

        if (scenario == 'evaluation' and station in 
                ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
                'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
                'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
                'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','CRM','EBK',
                'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
                'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
                'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
                'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
                'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
                'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
                'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
                'ZHWIN','ZHZEL'
                ]):
                continue
        if (scenario == 'historical' and station in 
                ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
                'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
                'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
                'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','BUF','CRM','EBK',
                'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
                'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
                'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
                'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
                'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
                'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
                'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
                'ZHWIN','ZHZEL','BEGTH','LUEMM','LUHOC','LULAN','LUMAL','LUROO',
                'LUSCH','LUSEM','LUSUR','LUWIL','LUWOL','SOBIB','SOGRE','SOMAT',
                'TIBED','TIOLI'
                ]):
                continue
            
        # masked_region = ch.where(ch == region_numbers[region], drop = True)
        check_lon = round(lon[i]/2,2)*2 ### ensure it is an even number as lat &
        check_lat = round(lat[i]/2,2)*2 ### lon in file are  even
        if check_lon in masked_region.lon and check_lat in masked_region.lat:
            idx_lon = np.where(masked_region.lon.values == check_lon)[0][0]
            idx_lat = np.where(masked_region.lat.values == check_lat)[0][0]
            # value_lon = masked_region.where(masked_region.lon == check_lon)
            # value = masked_region.where(masked_region.lat == check_lat)
            if masked_region.values[idx_lat][idx_lon]==region_numbers[region]:
                ax.plot(check_lon, check_lat, 'ko',markersize=2) 
                num_of_stations += 1

    ### --------------------------------------------------------------------###

    ax.text(0.97, 0.02, f'#of stations: {num_of_stations}',fontsize=9,
             weight='bold', 
            verticalalignment='bottom', horizontalalignment='right',
            rotation=0, transform=ax.transAxes)
    plt.savefig(os.path.join(plot_path, f'{season}_{region}.pdf'),
                    format='pdf',bbox_inches='tight')
    plt.close(fig)
