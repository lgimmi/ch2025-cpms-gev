#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
October 2023

Authors:
- Leandro Gimmi

Description:
Plot GEV skaling values for different seasons and scenarios and models

"""
import os
import numpy as np
import xarray as xr
import pandas as pd
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import cartopy.feature as cfeature
from matplotlib.ticker import ScalarFormatter
from matplotlib.colors import LinearSegmentedColormap
# from tqdm import tqdm
import matplotlib.patches as mpatches

'''-------------------------------------------------------------------------'''

### Choose between rcp85_moc / rcp85_eoc 
scenario = 'rcp85_moc'

### xHourly, xDaily
time_res = '1hr'

### Seasons: DJF, MAM, JJA, SON
season = 'JJA'

### CH2018 regions: CHNE, CHW, CHS ,CHAE or CHAW
## region = 'CHNE'

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)
return_periods = np.insert(return_periods,0,2)
# return_periods = np.append(return_periods, [150, 200, 250, 300])

'''-------------------------------------------------------------------------'''
plot_path = '/users/lgimmi/MeteoSwiss/figures/GEV_individual/scaling/'
path_region = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
### ------------------------------- CPM ----------------------------------- ###
path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}')
# path_save_GEV = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
#              +f'{scenario}')
# path_hist = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
#              +f'historical')

Institutes = ['obs','CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH','CLMcom-JLU',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom','MOHC','ICTP']
CPMs = ['obs','CCLM5-0-14','CCLM5-0-9','COSMO-crCLIM','CCLM5-0-15','CCLM5-0-15',
        'AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME','HadREM3-RA-UM10.1',
        'RegCM4-7']
### ------------------------------- RCM ----------------------------------- ###
RCM_path_hist = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
                +f'historical')
RCM_path_data = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
             +f'{scenario}')
RCM_Institutes = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
RCMs = ['COSMO-crCLIM','ALADIN63','RACMO23E','HCLIM38-ALADIN',
            'RegCM4-7']

'''-------------------------------------------------------------------------'''
region_numbers = {"CH":1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1, "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                 "SON": 'Autumn'}
if scenario == 'rcp85_moc':
    path_data = path_data[:-4] + '/moc'
    RCM_path_data = RCM_path_data[:-4] + '/moc'
elif scenario == 'rcp85_eoc':
    path_data = path_data[:-4] + '/eoc'
    RCM_path_data = RCM_path_data[:-4] + '/eoc'

for iterate, region in enumerate(region_numbers.keys()):
    print(f'{iterate+1}/{len(region_numbers.keys())}')
    fig = plt.figure(figsize=(15, 10))
    gs = fig.add_gridspec(nrows=3, ncols=4)
    row = 0
    col = 0
    if season == 'DJF':
        max_y = 20
    elif season == 'JJA':
        max_y = 20
    elif season == 'MAM':
        max_y = 20
    elif season == 'SON':
        max_y = 20

    if scenario == 'rcp85_moc':
        max_y = 40

    maxima_list = []
    for i_cpm, cpm in enumerate(Institutes):
        if cpm == 'obs':
            continue

        ax = fig.add_subplot(gs[row,col])

        # if scenario == 'rcp85_moc':
        #     path_data = path_data[:-4] + '/moc'
        # elif scenario == 'rcp85_eoc':
        #     path_data = path_data[:-4] + '/eoc'
        
        filename = (path_data + '/' + f'Skaling_{cpm}_{region}_{season}.npy')
        data = np.load(filename, allow_pickle=True)

        ten_perc, ninety_perc, median, mean, local = [], [], [], [], []
        
        for rp in return_periods:
            try:
                mean.append(np.nanmean(data[rp][0]))
                median.append(np.nanmean(data[rp][1]))
            except:
                breakpoint()
        
        for i in [ten_perc,ninety_perc,mean,median]:
            local.append(i)

        if cpm in RCM_Institutes:
            r_fname = (RCM_path_data + f'/Skaling_{cpm}_{region}_{season}.npy')
            r_data = np.load(r_fname, allow_pickle=True)

            r_ten_perc, r_ninety_perc, r_median, r_mean = [], [], [], []
            for rp in return_periods:
                r_mean.append(np.nanmean(r_data[rp][0]))
                r_median.append(np.nanmean(r_data[rp][1]))
            ax.plot(return_periods, r_median, label='parent RCM',
                    color='crimson',linestyle='-')

        # breakpoint()
        cc_relation = 7
        ax.plot(return_periods, np.full(len(return_periods),cc_relation),
                 color='k',linestyle='--',linewidth=1,alpha=0.8,
                 label='Clausius-Clapeyron relation')
        zero_line = 0
        ax.plot(return_periods, np.full(len(return_periods),zero_line),
                 color='k',linestyle='-',linewidth=1,alpha=0.6,)
        
        ax.plot(return_periods, median, label='CPM', color='mediumblue',
                 linestyle='-')

        ax.text(23, max_y+(1/15*max_y), f'{cpm}',fontsize=9,
                    verticalalignment='center', horizontalalignment='center',
                    rotation=0)

        major_ticks_y = np.arange(-max_y, max_y+1, 2*max_y/10*2)
        minor_ticks_y = np.arange(-max_y, max_y+1, max_y/10)
        ax.set_yticks(major_ticks_y)
        ax.set_yticks(minor_ticks_y, minor=True)

        ax.grid(which='both', axis='y', alpha=0.3)
        if row == 2:
            ax.set_xlabel('Returnperiod [yr]')
        elif row == 1 and col in [3]:
            ax.set_xlabel('Returnperiod [yr]')
        if col == 0:
            ax.set_ylabel(r'Scaling rate [$K^{-1}$]')
        
        ax.set_xscale('log')
        ax.set_xticks([2,5,10,20,50,100])
        ax.get_xaxis().set_major_formatter(ScalarFormatter())

        ax.set_ylim(-max_y+(max_y/10)*cc_relation,max_y)
        
        plt.xlim(return_periods[0],return_periods[-1]) 
        if col == 3:
            col = 0
            row += 1
        else:
            col += 1
        

    handles, labels = plt.gca().get_legend_handles_labels()
    order = [1,2,0]

    fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
            bbox_to_anchor=(0.91, 0.2), frameon=False, fontsize=12)
    plt.suptitle(f'{season}, {region}', y=0.93, fontsize=15)
    plt.suptitle(f'Scaling rate {scenario[-3:].capitalize()} | '
                 +f'{season_names[season]} | {region} | {time_res}',
                      y=0.93, fontsize=15)

    ### --------------------------------------------------------------------###
    ### Add overview of region and stations to plot
    ### --------------------------------------------------------------------###

    ax = fig.add_subplot(gs[row,col], projection=ccrs.PlateCarree())
    map_ext = np.array([5.2, 11.4, 45.0, 48.3])  # [degree]
    ax.coastlines(color='grey', linewidth=0.5)
    ax.add_feature(cfeature.BORDERS, edgecolor= 'black',
        linewidth = 0.5 )
    ax.set_extent(map_ext, crs=ccrs.PlateCarree())
    ax.set_aspect("auto")

    ### open region and station data
    path_stations = '/users/lgimmi/MeteoSwiss/data' 
    path_region = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
    filename_stations = (path_stations + '/station_metadat_rre150h0_1995_2010_'
                        +'allstats_andpartnerstats_withNA.csv')
    if region != 'CH':
        filename_regions = (path_region + '/station_region_latlon.nc')
    else:
        filename_regions = (path_region + 
                            '/station_region_latlon_maskCH.nc')

    regions = xr.open_dataset(filename_regions)
    if region != 'CH':
        ch = regions.orog
    else:
        ch = regions['mask_CH']
    data = pd.read_csv(filename_stations)
    lon = data['longitude']
    lat = data['latitude']

    colors = {"CH":"#da291c","CHAE": "darkkhaki", "CHAW": "darkgoldenrod",
            "CHW": "darkgreen", "CHNE": "green", "CHS": "gold"}

    cmap = LinearSegmentedColormap.from_list('custom_colormap', [colors[region],
                                            (1, 1, 1)] , N=2 )
    masked_region = ch.where(ch == region_numbers[region], drop=True) 
    col = ax.contourf(masked_region.lon, masked_region.lat, masked_region,
                    cmap=cmap, alpha=0.8)
    if region == 'CH': 
        col = ax.contourf(masked_region.lon, masked_region.lat, masked_region,
                    cmap=cmap, alpha=1.0)
        # ax.plot(masked_region.lon[114:115],masked_region.lat[53:54],marker='P',
        #         color='white', transform=ccrs.PlateCarree(),markersize=30)
    else:
        col = ax.contourf(masked_region.lon, masked_region.lat, masked_region,
                    cmap=cmap, alpha=0.8)

    ### --------------------------------------------------------------------###

    plt.savefig(os.path.join(plot_path, f'scaling_{scenario[-3:]}_GEV_{season}_{region}.pdf'),
                format='pdf',bbox_inches='tight')
    plt.close(fig)
