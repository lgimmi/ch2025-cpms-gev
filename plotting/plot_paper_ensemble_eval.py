#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
January 2024

Authors:
- Leandro Gimmi

Description:
Plot GEV values for different seasons and scenarios and models

"""
import os
import copy
import string
import numpy as np
import xarray as xr
import pandas as pd
import matplotlib as mpl
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import cartopy.feature as cfeature
from matplotlib.ticker import ScalarFormatter
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.patches as mpatches
import warnings
warnings.simplefilter("ignore")


'''-------------------------------------------------------------------------'''

### Scenario
scenario = 'evaluation'

### xHourly, xDaily
time_res = ['1d','1hr']

### Seasons: DJF, MAM, JJA, SON
seasons = ['JJA', 'DJF']
# seasons = ['MAM', 'SON']

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)

'''-------------------------------------------------------------------------'''
### ------------------------------- CPMs ---------------------------------- ###
plot_path = f'/users/lgimmi/MeteoSwiss/figures/paper/evaluation/'

path_region = '/scratch/snx3000/lgimmi/store/ALP-3/masks'

Institutes = ['CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH', 'CLMcom-JLU',
               'CLMcom-KIT','CNRM','KNMI','HCLIMcom', 'ensemble', 'stations']

region_numbers = {"CH":1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1,
                   "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                 "SON": 'Autumn'}

alphabet = list(string.ascii_lowercase)

'''-------------------------------------------------------------------------'''

for region in region_numbers.keys():
    ### Open Figure
    fig = plt.figure(figsize=(6, 5.4))
    gs = fig.add_gridspec(nrows=2, ncols=2)
    row, column, bet = 0, 0, 0
    print((f'Region: {region}').center(60,'~'))
    for season in seasons:
        print((f'Seasons: {season}').center(1,' '))
        for tres in time_res:

            if scenario == 'evaluation' and tres == '1hr':
                RCM_Institutes = ['CLMcom-BTU','CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
                RCMs = ['CCLM5-0-9','COSMO-crCLIM','ALADIN62','RACMO23E','HCLIM38-ALADIN',
                        'RegCM4-7']
            elif scenario == 'evaluation' and tres == '1d':
                # RCM_Institutes = ['CNRM','KNMI','HCLIMcom','ICTP']
                # RCMs = ['ALADIN62','RACMO23E','HCLIM38-ALADIN',
                #         'RegCM4-7']
                RCM_Institutes = ['CLMcom-BTU','CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
                RCMs = ['CCLM5-0-9','COSMO-crCLIM','ALADIN62','RACMO23E','HCLIM38-ALADIN',
                        'RegCM4-7']

            ax = fig.add_subplot(gs[row,column])
            if tres in ['1hr','1d']:
                divide = 1
            # elif tres in ['3hr','3d']:
            #     divide = 3
            # elif tres in ['6hr']:
            #     divide = 6
            # elif tres in ['5d']:
            #     divide = 5
            print((f'Time resolution: {tres}').center(40,' '))
            ensemble_list, ensemble_list_rcm = [], []
            for cpm in Institutes:   
                # if cpm == 'CLMcom-CMCC' and tres == '1d':
                #     continue
                path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{tres}/'
                                +f'pr_GEV/{scenario}')   
                RCM_path_data = (f'/scratch/snx3000/lgimmi/store/rcm/{tres}/pr_GEV/'
                     +f'{scenario}') 
                if cpm != 'ensemble' and cpm != 'stations':      
                    if scenario == 'rcp85_moc':
                        path_data = path_data[:-4] + '/moc'
                    elif scenario == 'rcp85_eoc':
                        path_data = path_data[:-4] + '/eoc'

                    filenmame = path_data + f'/{cpm}_{tres}_RP_prmax_{season}.nc'
                    ds = xr.open_dataset(filenmame)

                    ds_region = ds[region]
                    rp_index = ds_region.returnperiod.values
                    
                    lmean, lmedian, ltenP, lninetyP = [], [], [], []
                    for rp in return_periods:
                        rpidx = np.where(rp_index == rp)[0][0]
                        vmean = ds_region[rpidx][0]
                        vmedian = ds_region[rpidx][1]
                        vtenP = ds_region[rpidx][2]
                        vninetyP = ds_region[rpidx][3]

                        ### Use divide to get hourly values
                        lmean.append(np.nanmean(vmean) / divide)
                        lmedian.append(np.nanmean(vmedian) / divide)
                        ltenP.append(np.nanmean(vtenP) / divide)
                        lninetyP.append(np.nanmean(vninetyP) / divide)           
                        
                    ax.plot(return_periods, lmedian, label=f'Individual CPMs',
                            color='dodgerblue', linestyle='-', alpha=0.4,
                            linewidth=0.4,zorder=70)
                    
                    # ### Was not adjusted to be daily but is kg s-1m-2 smth like this
                    # if cpm in ['CLMcom-CMCC','CLMcom-ETH','CNRM'] and tres == '1d':
                    #     lmedian = np.array(lmedian) / 3600
                    print(cpm, "  ", lmedian[0])
                    ensemble_list.append(lmedian)

                    ### Add conventional regional climate models
                    if cpm in RCM_Institutes:
                        r_fname = (RCM_path_data + f'/Values_{cpm}_{region}_{season}.npy')
                        r_data = np.load(r_fname, allow_pickle=True)
                        r_ten_perc, r_ninety_perc, r_median, r_mean = [], [], [], []
            
                        for rp in return_periods:
                            r_value0, r_value1, r_value2, r_value3 = \
                                r_data[rp][0], r_data[rp][1], r_data[rp][2], r_data[rp][3]
                            r_ten_perc.append(np.nanmean(r_value2))
                            r_ninety_perc.append(np.nanmean(r_value3))
                            r_mean.append(np.nanmean(r_value0))
                            r_median.append(np.nanmean(r_value1))
                        ax.plot(return_periods, r_median, label=f'Individual RCMs',
                            color='crimson', linestyle='-', alpha=0.4,
                            linewidth=0.4, zorder=70)
                        ensemble_list_rcm.append(r_median)
    
                elif cpm == 'stations':
                    ### open region and station data
                    path_stations = '/users/lgimmi/MeteoSwiss/data' 
                    path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
                    if tres == '1hr':
                        fname_metadata = (path_stations + '/station_metadat_rre150h0_1995_2010_'
                                            +'allstats_andpartnerstats_withNA.csv')
                    elif tres == '1d':
                        fname_metadata = (path_stations + '/station_metadat_rre150d0_2000_'
                                            +'2009_allstatswithpartnerstats_allNA.csv')
                        
                    data = pd.read_csv(fname_metadata)
                    lon = data['longitude']
                    lat = data['latitude']

                    filename_station = (path_data + f'/Values_stations_{season}_'
                                        +f'{scenario}_{tres}.npy')
                    filename_region = (path_region_stations + 
                                    f'/stations_region_{scenario}_{tres}.csv')
                    station_data = np.load(filename_station, allow_pickle=True)
                    station_region = pd.read_csv(filename_region)
                    st_ten_perc, st_ninety_perc, st_median, st_mean, st_local = [],[],[],[], []

                    for rp in return_periods:
                        st1, st2, st3, st4 = [],[],[],[]
                        for i_station, station in enumerate(station_region.keys()):
                            if station_region.values[0][i_station] != region and region != 'CH':
                                continue
                            try:
                                st1.append(station_data[station][rp][2])
                                st2.append(station_data[station][rp][3])
                                st3.append(station_data[station][rp][0])
                                st4.append(station_data[station][rp][1])
                            except:
                                breakpoint()                        
                        st_ten_perc.append(np.nanmean(st1))
                        st_ninety_perc.append(np.nanmean(st2))
                        st_mean.append(np.nanmean(st3))
                        st_median.append(np.nanmean(st4))

                    ax.plot(return_periods, st_median, label=f'Stations',
                            color='black', linestyle='-', alpha=1,
                            linewidth=1.,zorder=100)
                    ax.plot(return_periods, st_ten_perc, label=r'10$^{th}$-90$^{th}$P',
                            color='black', linestyle='--', alpha=1,
                            linewidth=1.,zorder=80)
                    ax.plot(return_periods, st_ninety_perc, 
                            color='black', linestyle='--', alpha=1,
                            linewidth=1.,zorder=80)

                elif cpm == 'ensemble':
                    ensemble = np.mean(np.array(ensemble_list),axis=0)
                    # print(ensemble[0])
                    ensemble_rcm = np.mean(np.array(ensemble_list_rcm),axis=0)
                    ax.plot(return_periods, ensemble, label='Ensemble Median',
                            color='dodgerblue', linestyle='-', zorder=90)
                    ax.plot(return_periods, ensemble_rcm, label='Ensemble Median RCMs',
                            color='crimson', linestyle='-', zorder=90)
                    # ones = np.zeros(len(return_periods))
                    # ax.plot(return_periods, ones, color='black',
                    #         linewidth=0.8)
                    # ax.plot([-10,0], [0,0], label=f'Individual CPMs',
                    #     color='dodgerblue', linestyle='-', alpha=0.7,
                    #     linewidth=1, zorder=-100)
                    ax.plot([-10,0], [0,0], label=f'Individual CPMs',
                        color='dodgerblue', linestyle='-', alpha=0.7,
                        linewidth=1, zorder=-100)
                    ax.plot([-10,0], [0,0], label=f'Individual RCMs',
                        color='crimson', linestyle='-', alpha=0.7,
                        linewidth=1, zorder=-100)
           
            ### Plot settings
            if season == 'DJF' and tres == '1hr':
                max_y = 20
            elif season == 'SON' and tres == '1hr':
                max_y = 40
            elif season == 'DJF' and tres == '1d':
                max_y = 100
            elif season in ['JJA','MAM'] and tres == '1hr':
                max_y = 60
            elif season in ['JJA'] and tres == '1d':
                max_y = 120
            elif season in ['SON','MAM'] and tres == '1d':
                max_y = 140
            min_y = 0
            
            ax.set_title(f'{season_names[season]} | {tres}',fontsize=11,
                              loc='left')
            ax.set_title(f'{alphabet[bet]}',fontsize=10,
                              loc='right',x=0.98 )
            # if column == 0:
            #     props = dict(facecolor='white', alpha=0.5)
            #     ax.text(-0.27,0.35,f'{season_names[season]}',rotation=90,
            #         transform=ax.transAxes, fontsize=15)

            major_ticks_y = np.arange(-max_y, max_y+1, max_y/10*2)
            minor_ticks_y = np.arange(-max_y, max_y+1, max_y/10*1)
            if tres == '1hr':
                ax.set_ylabel('Intensity [mm/h]', fontsize=9,
                           labelpad=0.3)
            elif tres == '1d':
                ax.set_ylabel('Intensity [mm/d]', fontsize=9,
                           labelpad=0.2)
            ax.set_yticks(major_ticks_y)
            ax.set_yticks(minor_ticks_y, minor=True)
            ax.tick_params(axis='both', which='both', labelsize=8)
            ax.set_ylim(min_y,max_y)
            ax.grid(which='both', axis='y', alpha=0.3)
            
            ax.set_xscale('log')
            ax.set_xlabel('Returnperiod [yr]', fontsize=9)
                
            ax.set_xticks([5,10,20,50,100])
            ax.get_xaxis().set_major_formatter(ScalarFormatter())
            plt.xlim(return_periods[0],return_periods[-1])
            plt.suptitle(f' Evaluation of Ensemble Median | Region: {region} '
                         +'\n ERA-Interim Evaluation (2000-2009)',
                          fontsize=12, y=1.015)

            if column == 1:
                column = 0
                row += 1
            else:
                column += 1
            bet+=1

    handles, labels = plt.gca().get_legend_handles_labels()
    # order = [-4,-3,-6,-5,-2,-1]
    order = [-6,-5,-4,-3,-2,-1]

    fig.legend([handles[idx] for idx in order], [labels[idx] for idx in order],
            bbox_to_anchor=(0.86, 0.037),frameon=False, fontsize=8, ncols=3)

    plt.subplots_adjust( wspace=0.35, hspace=0.45)
    # plt.show()
    # breakpoint()
    plt.savefig(os.path.join(plot_path,
                 f'Paper_Ensemble_Eval_{region}_{seasons[0]}{seasons[1]}.pdf'),
                 format='pdf',bbox_inches='tight')

    plt.close()


