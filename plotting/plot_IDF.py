#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
December 2023

Authors:
- Leandro Gimmi

Description:
Intensity-Duration-Frequency Plot

"""
import os
import copy
import random
import warnings
import numpy as np
import xarray as xr 
import pandas as pd
import seaborn as sns
import matplotlib as mpl
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import cartopy.feature as cfeature
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
import matplotlib.transforms as transforms
from matplotlib.legend_handler import HandlerTuple
from matplotlib.colors import LinearSegmentedColormap
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


warnings.simplefilter("ignore")



'''-------------------------------------------------------------------------'''

### Choose return periods that should be plotted (optimized for 4)
rp_of_interest = [10,20,50,100]

time_res_list = ['1hr', '3hr', '6hr', '1d', '3d', '5d']

Institutes = ['EnsembleMean','CLMcom-ETH','CLMcom-BTU','CLMcom-CMCC','CLMcom-JLU',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom','stations']


seasons = ['JJA', 'DJF', 'SON', 'MAM']
title_name = {'rcp85_eoc': 'End of Century',
              'rcp85_moc': 'Mid of Century',
              'historical': 'Beginning of Century'}
### Use combination of regions
regions = {"CH":1, "CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1, "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                "SON": 'Autumn'}
time_period = ['historical','rcp85_eoc']

plot_path = f'/users/lgimmi/MeteoSwiss/figures/GEV_individual/IDF/'


### Needs to use yearly return periods
return_periods = np.arange(2,201,1)


'''-------------------------------------------------------------------------'''
### Set colors for plotting
if len(rp_of_interest) == 4:
    c_points = ['#EC2323','#AC3FCA', '#5230DE','#217FFE']
    c_points.reverse()
else:
    random.seed(7)
    c_points = []
    for c in range(len(rp_of_interest)):
        color = random.randrange(0, 2**24)
        hex_color = hex(color)
        c_points.append("#" + hex_color[2:])


for region in regions.keys():
    print(region)

    for i_season, season in enumerate(seasons):
        print(season.center(30,' '))
        
        sns.set_style("whitegrid",{'axes.edgecolor': '.15', 
                                   'grid.color': '#b0b0b0',
                                    'grid.linestyle': '--'})
        # sns.set(rc={'axes.facecolor':'white',
                    # 'grid.color': 'black'})
        

        # fig, ax = plt.subplots(figsize=(16.0 , 5))
        fig, axs = plt.subplots(figsize=(22.0 , 10), ncols=2, nrows=2)
        
        for cpm in Institutes:
            for scenario in time_period:
                if cpm == 'stations' and scenario != 'historical':
                    continue
                if scenario == 'historical':
                    ax,ax2 = axs[0,0],axs[1,0]
                elif scenario in ['rcp85_moc', 'rcp85_eoc']:
                    ax,ax2 = axs[0,1],axs[1,1]

                list_rp = []
                list_rp_cum = []
                ### Create List for data to be stored
                for i in range(len(rp_of_interest)):
                    list_rp.append([])
                    list_rp_cum.append([])

                for time_res in time_res_list:

                    ### Divide operator to get hourly value for all time_res
                    if time_res == '1hr':
                        divide = 1
                    elif time_res == '3hr':
                        divide = 3
                    elif time_res == '6hr':
                        divide = 6
                    elif time_res == '1d':
                        divide = 24
                    elif time_res == '3d':
                        divide = 72
                    elif time_res == '5d':
                        divide = 120

                    ### Open data   
                    path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
                        +f'{scenario}')
                    if scenario == 'rcp85_moc':
                        path_data = path_data[:-4] + '/moc'
                    elif scenario == 'rcp85_eoc':
                        path_data = path_data[:-4] + '/eoc'
                                    
                    if cpm != 'stations':
                        fname_data = (path_data + 
                                f'/OneYearStep_{cpm}_{time_res}_RP_prmax_{season}.nc')
                        ds = xr.open_dataset(fname_data)
                        da = ds[region]

                        for i_rp, rp in enumerate(rp_of_interest):
                            da_rp = da.sel(returnperiod=rp)[1] ### Median of all Bootstraps (1)
                            data = np.nanmean(da_rp)
                            ### Append and divide data [mm/h]
                            list_rp[i_rp].append(data / divide)
                            list_rp_cum[i_rp].append(data)
                        ds.close()

                    else:
                        fname_data = (path_data + 
                            f'/OneYearStep_Values_{cpm}_{season}_{scenario}_{time_res}.npy')
                        
                        data = np.load(fname_data, allow_pickle=True)
                        
                        for i_rp, rp in  enumerate(rp_of_interest):
                            stat_data = []
                            for stat_name in data.keys():
                                stat_data.append(data[stat_name][rp][1]) ### Median of all Bootstraps (1)
                            stat_mean  = np.nanmean(stat_data)
                            ### Append and divide data [mm/h]
                            list_rp[i_rp].append(stat_mean / divide)
                            list_rp_cum[i_rp].append(stat_mean)

                ### ------------------------------------------------------- ###
                ### Plotting IDF (1) and cummulative IDF (2)
                ### ------------------------------------------------------- ###
                    
                ### Create DataFrame with data for plotting with seaborn
                df = pd.DataFrame(list_rp, index=rp_of_interest,
                                  columns=time_res_list)
                df2 = pd.DataFrame(list_rp_cum, index=rp_of_interest,
                                    columns=time_res_list)
                if scenario == 'historical':
                    hist_df = copy.deepcopy(df)
                    cum_hist_df = copy.deepcopy(df2)
                    sns.stripplot(data=df, ax=ax, palette=c_points, marker=None,
                            log_scale=True, jitter=False, alpha=0.0)
                    sns.stripplot(data=df2, ax=ax2, palette=c_points, marker=None,
                            log_scale=True, jitter=False, alpha=0.0)
                    
                elif scenario in ['rcp85_moc', 'rcp85_eoc']:
                    df2 = (df - hist_df) 
                    sns.stripplot(data=df2, ax=ax2, palette=c_points, marker=None,
                            log_scale=False, jitter=False, alpha=0.0)
                    
                    df = (df - hist_df) / hist_df * 100
                    sns.stripplot(data=df, ax=ax, palette=c_points, marker=None,
                            log_scale=False, jitter=False, alpha=0.0)
                       
                
                offset = lambda p: transforms.ScaledTranslation(
                            p/72., 0, plt.gcf().dpi_scale_trans)
                # trans = plt.gca().transData
                trans = ax.transData
                trans2 = ax2.transData

                for idx,val in enumerate(df.index):
                    if cpm in ['EnsembleMedian','EnsembleMean']:
                        ax.scatter(df.columns.values, df.loc[val].values,
                                    color=c_points[idx],
                                marker='o', alpha=1.,zorder=100,label=val,
                                transform=trans+offset(-5))
                        ax.plot(df.loc[val].values, color=c_points[idx],
                                 alpha=1.,zorder=100,transform=trans+offset(-5))
                        
                        ### Cummulative IDF
                        # if scenario == 'historical':
                        ax2.scatter(df2.columns.values, df2.loc[val].values,
                                color=c_points[idx],
                            marker='o', alpha=1.,zorder=100,label=val,
                            transform=trans2+offset(-5))
                        ax2.plot(df2.loc[val].values, color=c_points[idx],
                                alpha=1.,zorder=100,transform=trans2+offset(-5))
                    
                    if cpm == 'stations':
                        ax.scatter(df.columns.values, df.loc[val].values,
                                    color=c_points[idx], marker='x', 
                                    alpha=0.8,zorder=110,s=50,label=val,
                                    transform=trans+offset(3))
                        # ax.plot(df.loc[val].values, color='k',
                        #          alpha=0.7,zorder=70,linewidth=1.0)

                        ### Cummulative IDF
                        if scenario == 'historical':
                            ax2.scatter(df2.columns.values, df2.loc[val].values,
                                    color=c_points[idx], marker='x', 
                                    alpha=0.8,zorder=110,s=50,label=val,
                                    transform=trans2+offset(3))
                    else:
                        ax.scatter(df.columns.values, df.loc[val].values,
                                    color=c_points[idx],
                                marker='o', alpha=0.1,zorder=100,
                                 transform=trans+offset(-5))
                        # facecolors='none',
                        ax.plot(df.loc[val].values, color=c_points[idx],
                                 alpha=0.2,zorder=50,linewidth=0.9, 
                                 linestyle='--',transform=trans+offset(-5))
                        
                        ### Cummulative IDF
                        # if scenario == 'historical':
                        ax2.scatter(df2.columns.values, df2.loc[val].values,
                                color=c_points[idx],
                            marker='o', alpha=0.1,zorder=100,
                                transform=trans2+offset(-5))
                        ax2.plot(df2.loc[val].values, color=c_points[idx],
                                alpha=0.2,zorder=50,linewidth=0.9, 
                                linestyle='--',transform=trans2+offset(-5))
                    
                    if scenario == 'rcp85_eoc':
                        ax.axvspan(ymin=0.4999, ymax=0.5, xmin = 0, xmax = 5,
                                   transform=ax.transAxes, color='k',
                                   linewidth=0.72) 
                        ax2.axvspan(ymin=0.25, ymax=0.25, xmin = 0, xmax = 5,
                                   transform=ax.transAxes, color='k',
                                   linewidth=0.72)      

                if cpm in ['EnsembleMean','EnsembleMedian']:
                    cpm_handles, cpm_labels = ax.get_legend_handles_labels()
                elif cpm == 'stations':
                    stat_handles, stat_labels = ax.get_legend_handles_labels()


        ### ------------------------------------------------------- ###
        ax,ax2,ax3,ax4 = axs[0,0],axs[1,0],axs[0,1],axs[1,1]
        ### axis settings of top left plot
        ax.set_ylim(0.1,81)
        ax.set_yticks([10,20,30,40,50], fontsize=10)
        ax.set_yticklabels([10,20,30,40,50], fontsize=10)
        ax.set_yticks([2,4,6,8,15], minor=True)
        ax.set_yticklabels([2,4,6,8,15],fontsize=7, minor=True)
        ax.grid(which='minor',alpha=0.9)

        ### axis settings of lower left plot
        ax2.set_ylim(10,500)
        ax2.set_yticks([50,100,200,500], fontsize=10)
        ax2.set_yticklabels([50,100,200,500], fontsize=10)
        ax2.set_yticks([10,20,30,75,120,150,300,400], minor=True)
        ax2.set_yticklabels([10,20,30,75,120,150,300,400],fontsize=7, minor=True)
        ax2.grid(which='minor',alpha=0.9)

        ### axis settings of top right plot
        ax3.set_ylim(-50,50)
        ax3.set_yticks(np.arange(-50,51,10), fontsize=10)
        ax3.set_yticklabels(np.arange(-50,51,10), fontsize=10)
        ax3.set_yticks(np.arange(-20,51,5), minor=True)
        # ax3.set_yticklabels([5,15,25,35,45,55,65,75,85,95],fontsize=7, minor=True)
        ax3.grid(which='minor',alpha=0.9)
        
        ### axis settings of lower right plot
        ax4.set_yticks(np.arange(-10,21,10), fontsize=10)
        ax4.set_yticklabels(np.arange(-10,21,10), fontsize=10)
        ax4.set_yticks(np.arange(-5,21,5), minor=True)
        # ax3.set_yticklabels([5,15,25,35,45,55,65,75,85,95],fontsize=7, minor=True)
        ax4.grid(which='minor',alpha=0.9)
        ax4.set_ylim(-5,15)

        ax2.set_xlabel('Duration', fontsize=14)
        ax4.set_xlabel('Duration', fontsize=14)
        ax.set_ylabel('Intensity [mm/h]', fontsize=14, labelpad=8)
        ax2.set_ylabel('Cummulative Intensity [mm]', fontsize=14, labelpad=8)
        ax3.set_ylabel('Relative Change [%]', fontsize=14, labelpad=8)
        ax4.set_ylabel('Absolute Change [mm/h]', fontsize=14, labelpad=8)

        ax.set_title('IDF | Beginning of Century', fontsize=16)
        ax3.set_title('Change until End of Century', fontsize=16)

        ### Reverse order of legend
        # handles, labels = ax.get_legend_handles_labels()
        # ax.legend(handles[::-1], labels[::-1], title='Return period [yr]')
        handles = []
        for i in range(len(cpm_handles)):
            handles.append((stat_handles[i+len(rp_of_interest)], 
                            stat_handles[i]))
            
        circ1 = Line2D([0], [0], linestyle="none", marker="o", alpha=1.,
                        markersize=6.3, markerfacecolor="k",
                        markeredgecolor="k")
        circ2 = Line2D([0], [0], linestyle="none", marker="x", alpha=1.,
                        markersize=6.5, markeredgecolor="k")
        circnone = Line2D([0], [0], linestyle="none", marker="o", alpha=1.,
                        markersize=6.5, color='#EAEAF2')
        handles_patch = handles[::-1]
        handles_patch.append((circnone, circ1))
        handles_patch.append((circnone, circ2))

        label_names = cpm_labels[::-1]
        label_names.append('CPMs')
        label_names.append('stations')
            
        ax.legend(handles_patch, label_names, title='Return period [yr]',
                    fontsize=10, handler_map={tuple: HandlerTuple(ndivide=None)})
        # ax.legend(handles_patch, label_names, title='Return period [yr]',
        #             fontsize=10, bbox_to_anchor=(2.1, -0.199),
        #             handler_map={tuple: HandlerTuple(ndivide=None)})

        # plt.suptitle(f'{region}  |  {season_names[season]}  |  '
        #              +f'{title_name[scenario]}', fontsize=16)
        plt.suptitle(f'{region}  |  {season_names[season]}', fontsize=20)


        ### ----------------------------------------------------------------###
        ### Add overview of region and stations to plot
        ### ----------------------------------------------------------------###
        def add_map_overview():
            ax4.remove()
            ax = fig.add_subplot(224, projection=ccrs.PlateCarree())
            map_ext = np.array([5.2, 11.4, 45, 48.3])  # [degree]
            rad_earth = 6371.0  # approximate radius of Earth [km]
            ### calc distortion of map due to map extent
            dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(
            map_ext[2:].mean()))) / 360.0 * (map_ext[1] - map_ext[0])
            dist_y = 2.0 * np.pi* rad_earth / 360.0 * (map_ext[3] - map_ext[2])

            ax.coastlines(color='grey', linewidth=0.5)
            ax.add_feature(cfeature.BORDERS, edgecolor= 'black',
                linewidth = 0.5 )
            ax.set_extent(map_ext, crs=ccrs.PlateCarree())
            # ax.set_aspect(dist_x/dist_y)
            ax.set_aspect(1.4)

            gl = ax.gridlines(crs= ccrs.PlateCarree(),
            linewidth=0.7, color='gray', alpha=0.5, linestyle='--') 
            gl.xformatter = LONGITUDE_FORMATTER 
            gl.yformatter = LATITUDE_FORMATTER
            gl.xlabel_style = {'size': 8, 'color': 'k','ha':'right'}
            gl.ylabel_style = {'size': 8, 'color': 'k','rotation':0}
            gl.left_labels = True
            gl.bottom_labels = True
            # gl.top_labels = True
            # gl.right_labels = True

            ### open region and station data
            path_stations = '/users/lgimmi/MeteoSwiss/data' 
            path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
            # if time_res == '1hr':
            #     filename_stations = (path_stations + '/station_metadat_rre150h0_1995_2010_'
            #                         +'allstats_andpartnerstats_withNA.csv')
            # elif time_res == '1d':
            filename_stations = (path_stations + '/station_metadat_rre150d0_2000_'
                                    +'2009_allstatswithpartnerstats_allNA.csv')
                
            if region != 'CH':
                filename_regions = (path_region_stations + '/station_region_latlon.nc')
            else:
                filename_regions = (path_region_stations + 
                                    '/station_region_latlon_maskCH.nc')

            regions_data = xr.open_dataset(filename_regions)
            if region != 'CH':
                ch = regions_data.orog
            else:
                ch = regions_data['mask_CH']
            data = pd.read_csv(filename_stations)
            lon = data['longitude']
            lat = data['latitude']

            colors = {"CH":"#da291c","CHAE": "darkkhaki", "CHAW": "darkgoldenrod",
                    "CHW": "darkgreen", "CHNE": "green", "CHS": "gold"}

            cmap = LinearSegmentedColormap.from_list('custom_colormap',
                                                      [colors[region],
                                                    (1, 1, 1)] , N=2 )
            
            masked_region = ch.where(ch == regions[region], drop=True)
            if region == 'CH': 
                col = ax.contourf(masked_region.lon, masked_region.lat, masked_region,
                            cmap=cmap, alpha=1.0)
            else:
                col = ax.contourf(masked_region.lon, masked_region.lat, masked_region,
                        cmap=cmap, alpha=0.8)

            ### Check in which region (rounded) lat/lon of stations are found
            for i, station in enumerate(data['nat_abbr']):

                if (scenario == 'evaluation' and station in 
                        ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
                        'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
                        'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
                        'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','CRM','EBK',
                        'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
                        'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
                        'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
                        'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
                        'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
                        'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
                        'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
                        'ZHWIN','ZHZEL'
                        ]):
                        continue
                if (scenario == 'historical' and station in 
                        ['AGAAB','AGBRW','AGFAW','AGFRI','AGKST','AGLUP','AGREH','AGTGI',
                        'AGWDS','AGWOH','AND','BEBIL','BEBUD','BEBUE','BEBUV','BEHAB',
                        'BEHAK','BEINS','BEKAP','BEKRA','BEKRL','BEKSE','BELAT','BER',
                        'BERIF','BESCB','BEVIL','BEZ','BIE','BIZ','BOU','BUF','CRM','EBK',
                        'EGO','FEY','GEARE','GEDDU','GEERM','GEMIR','GESAV','GIH','GRE',
                        'HAI','HIR','HOE','MOE','SOERS','SOGER','SOHIM','SOMET','SOSWE',
                        'SPF','TIARO','TIBIA','TIBIG','TICAN','TICAR','TICHI','TICIA',
                        'TICMD','TICOL','TICVM','TIFUS','TIGIU','TIGNO','TIGRA','TIISO',
                        'TIMAG','TIMEN','TINOV','TIOLV','TISON','VSANZ','VSBAS','VSCHY',
                        'VSJEI','VSMAT','VSSAB','VSSAL','VSVER','ZHBAM','ZHBID','ZHEGG',
                        'ZHHOF','ZHMEM','ZHMON','ZHNEU','ZHNIE','ZHNUR','ZHRAT','ZHTUR',
                        'ZHWIN','ZHZEL','BEGTH','LUEMM','LUHOC','LULAN','LUMAL','LUROO',
                        'LUSCH','LUSEM','LUSUR','LUWIL','LUWOL','SOBIB','SOGRE','SOMAT',
                        'TIBED','TIOLI'
                        ]):
                        continue
                    
                check_lon = round(lon[i]/2,2)*2 ### ensure it is an even number as lat &
                check_lat = round(lat[i]/2,2)*2 ### lon in file are  even
                if check_lon in masked_region.lon and check_lat in masked_region.lat:
                    idx_lon = np.where(masked_region.lon.values == check_lon)[0][0]
                    idx_lat = np.where(masked_region.lat.values == check_lat)[0][0]
                    
                    if masked_region.values[idx_lat][idx_lon]==regions[region]:

                        # if time_res == '1hr':
                        #     ax.plot(check_lon, check_lat, 'ko',markersize=2) 
                        # elif time_res == '1d':
                        ax.plot(check_lon, check_lat, 'kx',markersize=2)

            fig.axes[3].set_position([0.52,0.105,0.27,0.27*1.3])
        
        # add_map_overview()

        # plt.savefig(os.path.join(plot_path, 
        #     f'IDF_{region}_{season}_{scenario}.pdf'), format='pdf',
        #     bbox_inches='tight')
        sns.despine()

        plt.show()
        breakpoint()
    
        if 'EnsembleMean' in Institutes:
            plt.savefig(os.path.join(plot_path, 
                f'IDFs_{region}_{season}.pdf'), format='pdf',
                bbox_inches='tight')
        elif 'EnsembleMedian' in Institutes:
            plt.savefig(os.path.join(plot_path, 
                f'IDFsMedian_{region}_{season}.pdf'), format='pdf',
                bbox_inches='tight')
        else:
            print('Please add EnsembleMean or EnsembleMedian to Institutes')
            breakpoint()
        
    
            

