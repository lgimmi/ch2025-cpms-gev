#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
November 2023

Authors:
- Leandro Gimmi

Description:
Map multimodel mean for every returnperiod

"""

import os
import copy
import warnings
import xesmf as xe
import numpy as np
import xarray as xr 
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import cartopy.feature as cfeature
from matplotlib.colors import LinearSegmentedColormap, ListedColormap
warnings.simplefilter("ignore")

'''-------------------------------------------------------------------------'''

### Choose between rcp85_moc / rcp85_eoc
scenario = 'rcp85_eoc'

### xHourly, xDaily
time_res = '1d'

### Choose stat from ['Mean','Median','10P','90P'] as indexnumber (0-3)
### Standard is Median (1)
stat = 1
### uncertauinty is shown as the fraction of the 90-quartile and the median

### Define Returnperiods for which values GEV values should be calculated
return_periods = [2,5,10,20,30,50,100,200]
# return_periods = [100]
# return_periods = np.arange(5,105,5)
# return_periods = np.insert(return_periods,0,2)
# return_periods = np.append(return_periods, [150, 200, 250, 300])
# rp_list = np.arange(2,201,1)


seasons = [ 'DJF', 'JJA',  'SON', 'MAM']
# seasons = [ 'JJA']

title_name = {'rcp85_eoc': 'End of century',
              'rcp85_moc': 'Mid of century'}
### Use combination of regions
regions = {"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1, "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                "SON": 'Autumn'}

### ------------------------------- CPM ----------------------------------- ###

path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}')
path_data_hist = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'historical')


Institutes = ['CLMcom-BTU','CLMcom-ETH','CLMcom-CMCC','CLMcom-JLU',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom','EnsembleMean']
Institutes_scaling = ['CLMcom-ETH','CLMcom-BTU','CLMcom-CMCC',
                      'CLMcom-JLU', 'CLMcom-KIT','CNRM','KNMI','HCLIMcom',
                      'EnsembleMean']

rcm = None

### ------------------------------- RCM ----------------------------------- ###
# path_data = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
#              +f'{scenario}')
# path_data_hist = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
#              +f'historical')

# Institutes = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom','EnsembleMean']
# Institutes_scaling = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom','EnsembleMean']

# rcm = 1

def fill_patches_with_NNmean(arr, patch_size=5, dimension='1D'):
    '''
    Fixes patches created by regridding of models
    '''
    from scipy.interpolate import griddata

    filled_arr = np.copy(arr)

    # Find nan to values transitions which are not at the border, i.e.
    # patches to be filled
    if dimension == '1D':
        patches = \
            np.argwhere(np.isnan(arr) & 
                        ~np.isnan(np.roll(arr,patch_size)) &
                        ~np.isnan(np.roll(arr,-patch_size)))

    elif dimension == '2D':
        patches = \
            np.argwhere(np.isnan(arr) & 
                        ~np.isnan(np.roll(arr,[patch_size, patch_size],
                                            axis=(0, 1))) &
                        ~np.isnan(np.roll(arr,[-patch_size, -patch_size], 
                                            axis=(0, 1))))
    
    try:
    # Interpolate using griddata where we have patches
        filled_arr[tuple(zip(*patches))] = \
            griddata(np.array(np.where(~np.isnan(arr))).T,
                    arr[~np.isnan(arr)],
                    tuple(patches.T),
                    method='nearest')
    except:
        pass
    
    return filled_arr

modelagreement = []
modelagreement_neg = []
for i_cpm, cpm in enumerate(Institutes):
    if cpm != 'EnsembleMean':
        modelagreement.append([])
        modelagreement_neg.append([])
        if i_cpm != 0:
            modelagreement[i_cpm-1] = np.asarray(modelagreement[i_cpm-1])
            modelagreement_neg[i_cpm-1] = np.asarray(modelagreement_neg[i_cpm-1])
    if time_res == '1hr':
        plot_path = f'/users/lgimmi/MeteoSwiss/figures/GEV_individual/maps/{cpm}/{scenario}/'
    elif time_res == '1d':
        plot_path = f'/users/lgimmi/MeteoSwiss/figures/day/maps/{cpm}/{scenario}/'

    if scenario == 'rcp85_moc':
        path_data = path_data[:-4] + '/moc'
        plot_path = plot_path[:-11] + '/moc'
    elif scenario == 'rcp85_eoc':
        path_data = path_data[:-4] + '/eoc'
        plot_path = plot_path[:-11] + '/eoc'
    '''-------------------------------------------------------------------------'''

    levels_change_signal = np.arange(-100,101,10)

    for i_season, season in enumerate(seasons):
        print(f'{i_season+1}/{len(seasons)}')
            
        if cpm != 'EnsembleMean':
            modelagreement[i_cpm].append([])
            modelagreement_neg[i_cpm].append([])
            
            ### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            ### All return periods individually and combine regions
            ### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            filename = (path_data + 
                        f'/Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
            filename_hist  = (path_data_hist + 
                        f'/Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')

            try:
                ds = xr.open_mfdataset(filename)
                ds_hist = xr.open_mfdataset(filename_hist)
            except:
                breakpoint()   
        else:
            filename = (path_data + 
                        f'/{cpm}_{time_res}_RP_prmax_{season}.nc')
            filename_hist  = (path_data_hist + 
                              f'/{cpm}_{time_res}_RP_prmax_{season}.nc')
            ds = xr.open_mfdataset(filename)
            ds_hist = xr.open_mfdataset(filename_hist)

        ### Open scaling stuff
        if cpm in Institutes_scaling:
            filename_scaling = (path_data + 
                                f'/{cpm}_scaling_{time_res}_RP_prmax_{season}.nc')
            ds_scaling = xr.open_mfdataset(filename_scaling)
        
        for i_rp, rp in enumerate(return_periods):
            # if i_rp != 0:
            #     continue
            # if cpm != 'EnsembleMean':
                # modelagreement[i_cpm][i_season].append([])
                # modelagreement_neg[i_cpm][i_season].append([])
            i_rp_ma = copy.deepcopy(i_rp)
            print(rp)
            # cc_map_list = []

            # for i_cpm, cpm in enumerate(Institutes):
            #     ### open data
            #     # filename_rcp = (path_data + 
            #     #             f'/Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
            #     # filename_hist = (path_data_hist + 
            #     #             f'/Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
            #     # ds_rcp = xr.open_mfdataset(filename_rcp)
            #     # ds_hist = xr.open_mfdataset(filename_hist)

            #     for p_i, p_reg in enumerate(regions.keys()):
            #         existing_rp = ds[p_reg]['returnperiod'].values.tolist()
            #         i_rp = existing_rp.index(rp)
            #         hist_value = ds_hist[p_reg][i_rp][stat].values
            #         rcp_value = ds[p_reg][i_rp][stat].values
            #         # Change signal in percent
            #         change_signal = ((rcp_value - hist_value) / hist_value * 100)

            #         if p_i == 0:
            #             cc_map = change_signal

            #         else:
            #             cc_map = np.where(np.isnan(cc_map) == True,
            #                             change_signal, cc_map)
            #     cc_map_list.append(cc_map)

            # cc_ensemble = np.nanmean(np.array(cc_map_list), axis=0)

            # cc_ensemble = fill_patches_with_NNmean(cc_ensemble)

            
            ### Calculate change signal for every region
            for p_i, p_reg in enumerate(regions.keys()):
                # if cpm != 'EnsembleMean':
                existing_rp = ds[p_reg]['returnperiod'].values.tolist()
                i_rp = existing_rp.index(rp)
                
                hist_value = ds_hist[p_reg][i_rp][stat].values
                rcp_value = ds[p_reg][i_rp][stat].values
                # Change signal in percent
                change_signal = ((rcp_value - hist_value) / hist_value * 100)
                if cpm in Institutes_scaling:
                    ### Scaling
                    rp_scaling = ds_scaling[p_reg]['returnperiod'].values.tolist()
                    i_rp_scaling = rp_scaling.index(rp)
                    try:
                        scaling = ds_scaling[p_reg][i_rp_scaling][stat].values
                    except:
                        breakpoint()

                if p_i == 0:
                    # if cpm != 'EnsembleMean':
                    cc_map = change_signal
                    if cpm in Institutes_scaling:
                        scaling_map = scaling

                else:
                    # if cpm != 'EnsembleMean':
                    cc_map = np.where(np.isnan(cc_map) == True,
                                change_signal, cc_map)
                    if cpm in Institutes_scaling:
                        scaling_map = np.where(np.isnan(scaling_map) == True,
                                    scaling, scaling_map)
            
            
            if rcm == None:
                cc_ensemble = fill_patches_with_NNmean(cc_map, 8, '2D')
                cc_ensemble = fill_patches_with_NNmean(cc_ensemble,8)
            else:
                cc_ensemble = fill_patches_with_NNmean(cc_map, 10, '2D')
                cc_ensemble = fill_patches_with_NNmean(cc_ensemble,10)
            
            if cpm != 'EnsembleMean':
                ### Create new mask of switzerland for modelagreement
                positives = np.where(np.isnan(cc_ensemble) == True, np.nan, 0)
                negatives = np.where(np.isnan(cc_ensemble) == True, np.nan, 0)

                ### Append Bool mask where values are positive
                try:
                    modelagreement[i_cpm][i_season].append(cc_ensemble > 0)
                    modelagreement_neg[i_cpm][i_season].append(cc_ensemble < 0)
                except:
                    breakpoint()
                
            else:
                for i_Inst in range(len(Institutes)-1):
                    if i_Inst == 0:
                        positives = (modelagreement[i_Inst][i_season][i_rp_ma]*1)
                        negatives = (modelagreement_neg[i_Inst][i_season][i_rp_ma]*1)
                    else:
                        positives += (modelagreement[i_Inst][i_season][i_rp_ma]*1)
                        negatives += (modelagreement_neg[i_Inst][i_season][i_rp_ma]*1)

            if cpm in Institutes_scaling:
                if rcm == None:
                    scaling_ensemble = fill_patches_with_NNmean(scaling_map,
                                                        dimension='2D')
                    scaling_ensemble = fill_patches_with_NNmean(
                                                        scaling_ensemble, 8)
                else:
                    scaling_ensemble = fill_patches_with_NNmean(scaling_map,10,
                                                        dimension='2D')
                    scaling_ensemble = fill_patches_with_NNmean(
                                                        scaling_ensemble, 10)

            # '''-----------------------------------------------------------'''
            # ## Plotting information
            # '''-----------------------------------------------------------'''

            ### MMM is in rotated lat lon grid, get information
            rlat,rlon = ds.lat, ds.lon
            # if cpm not in ['CNRM','KNMI','HCLIMcom','MOHC','ICTP']:
            #     rlat,rlon,pole = ds.rlat, ds.rlon, ds.rotated_pole
            #     pole_lon = pole.attrs['grid_north_pole_longitude']
            #     pole_lat = pole.attrs['grid_north_pole_latitude']
            #     crs = ccrs.RotatedPole(pole_longitude=pole_lon,
            #                             pole_latitude=pole_lat)
            # elif cpm == 'MOHC':
            #     rlat,rlon,pole = ds.rlat, ds.rlon, ds.rotated_latitude_longitude
            #     pole_lon = pole.attrs['grid_north_pole_longitude']
            #     pole_lat = pole.attrs['grid_north_pole_latitude']

            #     crs = ccrs.RotatedPole(pole_longitude=pole_lon,
            #                             pole_latitude=pole_lat)

            # else:
            #     rlat,rlon = ds.lat, ds.lon

            ### Set map extent
            # map_ext = np.array([-180, 180, -90, 90])  # [degree]
            # map_ext = np.array([-0, 11, 45, 49])  # [degree]
            map_ext = np.array([5.2, 11.4, 45, 48.3])  # [degree]
            rad_earth = 6371.0  # approximate radius of Earth [km]
            dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(
                map_ext[2:].mean()))) / 360.0 * (map_ext[1] - map_ext[0])
            dist_y = 2.0 * np.pi* rad_earth / 360.0 * (map_ext[3] - map_ext[2])


            ### ---------------------- Change-Signal ---------------------- ###
            ### Create figure that is adjusted for latitudinal distortion
            fig, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
                                subplot_kw={'projection':ccrs.PlateCarree()})

            ax.coastlines(color='black', linewidth=0.5)
            ax.add_feature(cfeature.BORDERS, edgecolor='black', linewidth=0.5)
            ax.set_aspect("auto")
            ax.set_extent(map_ext, crs=ccrs.PlateCarree())
            norm = mpl.colors.TwoSlopeNorm(vmin=-51, vcenter=0., vmax=51)
            # left = np.linspace(-48,-0.01,8)
            # right = np.linspace(0.01,48,8)
            # norm_array = np.append(left,right)
            # norm = mpl.colors.BoundaryNorm(norm_array, 14)

            ### remove frame of figure
            fig.patch.set_visible(False)
            ax.axis('off')  

            colaray = np.linspace(0,0.4375,8)
            # white = np.arange(0.5,0.6,1)
            white = [1.0,1.0,1.0,1.0]
            # colaray = np.append(colaray, white)
            colaray_pos = np.linspace(0.5625,1,8)
            colaray = np.append(colaray, colaray_pos)
            color_array = cm.BrBG(colaray)
            color_array = np.insert(color_array, 8, white ,axis=0)
            cmap = ListedColormap(color_array)

            col = ax.pcolormesh(rlon, rlat, cc_ensemble, cmap=cmap,
                                    norm=norm, rasterized=True)

            if cpm == 'EnsembleMean':
                # if season == 'JJA' and rp == 100:
                #     breakpoint()
                limit = len(Institutes) - 2
                if rcm == 1:
                    hatch = np.where(positives < limit, 1, np.nan)
                    hatch_neg = np.where(negatives < limit, 1, np.nan)
                    # hatch = np.ma.masked_where(positives > 3,
                    #                             positives, copy=True)
                    # hatch_neg = np.ma.masked_where(negatives > 3,
                    #                             negatives, copy=True)
                elif rcm == None:
                    hatch = np.where(positives < limit , 1, np.nan) 
                    hatch_neg = np.where(negatives < limit, 1, np.nan)
                mpl.rcParams['hatch.linewidth'] = 0.5
                # ax.contourf(rlon, rlat, hatch, alpha=0.2, color='whitesmoke')
                # ax.contourf(rlon, rlat, hatch_neg, alpha=0.2,
                #              color='whitesmoke')
                main_hatch = hatch * hatch_neg
                new_main_hatch = np.where(np.isnan(cc_ensemble) == True, 
                                       np.nan, main_hatch)
                ax.pcolor(rlon, rlat, new_main_hatch, hatch='//', alpha=0.,
                          edgecolor='k', linewidth=0.1)
                # ax.pcolor(rlon, rlat, hatch, hatch='//', alpha=0.,
                #            edgecolor='r', linewidth=0.1)
                
                
            ### Add text box with regional mean
            regional_mean = np.nanmean(cc_ensemble)
            props = dict(boxstyle='Square', facecolor='white', alpha=0.5)
            ax.text(0.02,0.95,f'Regional mean: {regional_mean:.2f}%',transform=ax.transAxes,
                     bbox=props, fontsize=16)
            
            if cpm == 'EnsembleMean':
                ### Add explanation of hatching
                props2 = dict(boxstyle='Square', facecolor='white',
                            edgecolor='white', alpha=0.)
                ax.text(0.77,0.08,f'Color', transform=ax.transAxes, bbox=props,
                        fontsize=10)
                ax.text(0.82,0.08,f'High Model Agreement',
                         transform=ax.transAxes, bbox=props2, fontsize=10)
                ax.text(0.77,0.04,f'  ///// ', transform=ax.transAxes,
                         bbox=props, fontsize=10)
                ax.text(0.82,0.04,f'Low model agreement',
                         transform=ax.transAxes, bbox=props2, fontsize=10)
            
            cbar_ax= fig.add_axes([0.92,0.122,0.03,.75]) # vertical
            # cbar_ax= fig.add_axes([0.15,0.08,0.75,.03]) # horizontal
            minorticks = np.arange(-48, 49, 3)
            majorticks = np.arange(-48, 49, 6)
            cbar = mpl.colorbar.ColorbarBase(cbar_ax, cmap=cmap,
                                    norm=norm,
                                    extend='both',
                                    ticks=majorticks,
                                    spacing='proportional',
                                    orientation='vertical')
            cbar.ax.tick_params(labelsize=10)
            cbar.ax.yaxis.set_ticks(minorticks, minor=True)
            cbar.ax.tick_params(size=0,which=u'both') # remove ticks

            if cpm == 'EnsembleMean':
                plt.suptitle((f'Change in max. 60min precip. at {title_name[scenario]} '
                            +f'for a {rp}-year return period [%], \n' 
                            +f'{season_names[season]} |'
                            +' Ensemble Mean'), y=0.95, fontsize=17)
            else:
                plt.suptitle((f'Change in max. 60min precip. at {title_name[scenario]} '
                        +f'for a {rp}-year return period [%], \n' 
                        +f'{season_names[season]} |'
                        +f' {cpm}'), y=0.95, fontsize=17)
            
            if rcm == None:
                plt.savefig(os.path.join(plot_path, 
                        f'Change_{scenario[-3:]}_{season}_RP{rp}.pdf'),
                        format='pdf',bbox_inches='tight')
            elif rcm == 1:
                plt.savefig(os.path.join(plot_path, 
                        f'RCM_Change_{scenario[-3:]}_{season}_RP{rp}.pdf'),
                        format='pdf',bbox_inches='tight')

            plt.close()


            if cpm in Institutes_scaling:
                ### ---------------------- Scaling ---------------------- ###
                ### Calculate where Claus.-Clapeyron relation is seen (==7%/°K)
                CC_ensemble = np.where((scaling_ensemble >= 6.5) & (scaling_ensemble < 7.5),
                                        scaling_ensemble, np.nan)
                rlat,rlon = ds_scaling.lat, ds_scaling.lon


                ### Create figure that is adjusted for latitudinal distortion
                fig2, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
                                    subplot_kw={'projection':ccrs.PlateCarree()})

                ax.coastlines(color='black', linewidth=0.5)
                ax.add_feature(cfeature.BORDERS, edgecolor='black', linewidth=0.5)
                ax.set_aspect("auto")
                ax.set_extent(map_ext, crs=ccrs.PlateCarree())
                # norm2 = mpl.colors.TwoSlopeNorm(vmin=-10., vcenter=0., vmax=45)
                # norm2 = mpl.colors.BoundaryNorm([-10,-5,0,5,10,15,20,25,30,35,40,45], 12)
                norm2 = mpl.colors.BoundaryNorm(np.linspace(-8,44,14), 14)
                # breakpoint()
                col_a = (cm.BrBG(np.linspace(0.2,0.45,3))).tolist()[:2]
                col_b = (cm.BrBG(np.linspace(0.5,1,11))).tolist()
                # col_c = (cm.BrBG(np.linspace(0.5,0.5,1))).tolist()
                # col_a.extend(col_c)
                col_a.extend(col_b)
                col_array = np.array(col_a)
                cmap2 = ListedColormap(col_array)
                # ax.contourf(rlon, rlat, scaling_ensemble,100,rasterized=True)
                col2 = ax.pcolormesh(rlon, rlat, scaling_ensemble, cmap=cmap2,
                                     norm=norm2, rasterized=True)
                if cpm == 'EnsembleMean':
                    limit = len(Institutes) - 2
                    if rcm == 1:
                        hatch = np.where(positives < limit, 1, np.nan)
                        hatch_neg = np.where(negatives < limit, 1, np.nan)
                    elif rcm == None:
                        hatch = np.where(positives < limit , 1, np.nan) 
                        hatch_neg = np.where(negatives < limit, 1, np.nan)
                    mpl.rcParams['hatch.linewidth'] = 0.5
                    
                    main_hatch = hatch * hatch_neg
                    new_main_hatch = np.where(np.isnan(cc_ensemble) == True, 
                                       np.nan, main_hatch)
                    ax.pcolor(rlon, rlat, new_main_hatch, hatch='//', alpha=0.,
                            edgecolor='k', linewidth=0.1)
                    
                
                ### Add text box with regional mean
                regional_mean = np.nanmean(scaling_ensemble)
                props = dict(boxstyle='Square', facecolor='white', alpha=0.5)
                ax.text(0.02,0.95,f'Regional mean: {regional_mean:.2f}%/°K',
                        transform=ax.transAxes,
                        bbox=props, fontsize=16)
                if cpm == 'EnsembleMean':
                    ### Add explanation of hatching
                    props2 = dict(boxstyle='Square', facecolor='white',
                                edgecolor='white', alpha=0.)
                    ax.text(0.77,0.08,f'Color', transform=ax.transAxes,
                             bbox=props, fontsize=10)
                    ax.text(0.82,0.08,f'High Model Agreement',
                             transform=ax.transAxes, bbox=props2, fontsize=10)
                    ax.text(0.77,0.04,f'  ////  ', transform=ax.transAxes,
                             bbox=props, fontsize=10) 
                    ax.text(0.82,0.04,f'Low model agreement',
                             transform=ax.transAxes, bbox=props2, fontsize=10)

                cbar_ax2 = fig2.add_axes([0.92,0.122,0.03,.75])
                minorticks = np.arange(-8, 44, 4)
                majorticks = np.arange(-8, 44, 8)
                cbar2 = mpl.colorbar.ColorbarBase(cbar_ax2, 
                                        cmap=cmap2,
                                        norm=norm2,
                                        extend='both',
                                        ticks=majorticks,
                                        spacing='proportional',
                                        orientation='vertical')
                cbar2.ax.tick_params(labelsize=10)
                cbar2.ax.yaxis.set_ticks(minorticks, minor=True)

                if cpm == 'EnsembleMean':
                    plt.suptitle((f'Scaling of max. 60min precip. at {title_name[scenario]} '
                                +f'for a {rp}-year return period [%/°K], \n' 
                                +f'{season_names[season]} |'
                                +' Ensemble Mean'), y=0.95, fontsize=17)
                else:
                    plt.suptitle((f'Scaling of max. 60min precip. at {title_name[scenario]} '
                            +f'for a {rp}-year return period [%/°K], \n' 
                            +f'{season_names[season]} |'
                            +f' {cpm}'), y=0.95, fontsize=17)
                    
                if rcm == None:
                    plt.savefig(os.path.join(plot_path, 
                            f'Scaling_{scenario[-3:]}_{season}_RP{rp}.pdf'),
                            format='pdf',bbox_inches='tight')
                elif rcm == 1:
                    plt.savefig(os.path.join(plot_path, 
                            f'RCM_Scaling_{scenario[-3:]}_{season}_RP{rp}.pdf'),
                            format='pdf',bbox_inches='tight')
        
                plt.close()