#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
December 2023

Authors:
- Leandro Gimmi

Description:
Plot temperature change map used in tas

"""
import os
import warnings
import xesmf as xe
import numpy as np
import xarray as xr 
import pandas as pd
import matplotlib as mpl
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
import cartopy.feature as cfeature
from matplotlib.colors import LinearSegmentedColormap, ListedColormap
warnings.simplefilter("ignore")

'''-------------------------------------------------------------------------'''

### Choose between rcp85_moc / rcp85_eoc
scenario = 'rcp85_eoc'

### xHourly, xDaily
time_res = '1hr'


seasons = ['JJA', 'DJF', 'SON', 'MAM']
title_name = {'rcp85_eoc': 'End of century',
              'rcp85_moc': 'Mid of century'}
### Use combination of regions
regions = {"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1, "CHS": 3}

season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                "SON": 'Autumn'}

path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}')
path_data_hist = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'historical')

Institutes = ['EnsembleMean','CLMcom-BTU','CLMcom-ETH','CLMcom-JLU',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom','MOHC','ICTP']
Institutes_tas = ['EnsembleMean','CLMcom-ETH','CLMcom-BTU','CLMcom-JLU',
              'CLMcom-KIT','HCLIMcom']

def fill_patches_with_NNmean(arr, patch_size=5, dimension='1D'):
    '''
    Fixes patches created by regridding of models
    '''
    from scipy.interpolate import griddata

    filled_arr = np.copy(arr)

    # Find nan to values transitions which are not at the border, i.e.
    # patches to be filled
    if dimension == '1D':
        patches = \
            np.argwhere(np.isnan(arr) & 
                        ~np.isnan(np.roll(arr,patch_size)) &
                        ~np.isnan(np.roll(arr,-patch_size)))

    elif dimension == '2D':
        patches = \
            np.argwhere(np.isnan(arr) & 
                        ~np.isnan(np.roll(arr,[patch_size, patch_size],
                                            axis=(0, 1))) &
                        ~np.isnan(np.roll(arr,[-patch_size, -patch_size], 
                                            axis=(0, 1))))
    
    # breakpoint()
    # Interpolate using griddata where we have patches
    filled_arr[tuple(zip(*patches))] = \
            griddata(np.array(np.where(~np.isnan(arr))).T,
                    arr[~np.isnan(arr)],
                    tuple(patches.T),
                    method='nearest')
    
    return filled_arr


for cpm in Institutes_tas:
    if cpm == 'EnsembleMean':
        continue

    plot_path = f'/users/lgimmi/MeteoSwiss/figures/day/maps/{scenario}/'

    if scenario == 'rcp85_moc':
        path_data = path_data[:-4] + '/moc'
        plot_path = plot_path[:-11] + '/moc'
    elif scenario == 'rcp85_eoc':
        path_data = path_data[:-4] + '/eoc'
        plot_path = plot_path[:-11] + '/eoc'
    '''-------------------------------------------------------------------------'''

    levels_change_signal = np.arange(-100,101,10)

    for i_season, season in enumerate(seasons):
        print(f'{i_season+1}/{len(seasons)}')
        
        ### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        ### Combine regions
        ### - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

        ### Open tas stuff
        if cpm in Institutes_tas:
            filename_tas = (path_data + 
                                f'/{cpm}_tas_{time_res}_RP_prmax_{season}.nc')
            ds_temp = xr.open_mfdataset(filename_tas)

        ### Calculate change signal for every region
        for p_i, p_reg in enumerate(regions.keys()):
            tas = ds_temp[p_reg].values

            if p_i == 0:
                tas_map = tas

            else:
                tas_map = np.where(np.isnan(tas_map) == True,
                                tas, tas_map)
        tas_ensemble = tas_map
        try:
            tas_ensemble = fill_patches_with_NNmean(tas_map,
                                                        dimension='2D')
            tas_ensemble = fill_patches_with_NNmean(tas_ensemble,8)
        except:
            pass

        # breakpoint()
        # '''-----------------------------------------------------------'''
        # ## Plotting information
        # '''-----------------------------------------------------------'''

        ### MMM is in rotated lat lon grid, get information
        if cpm not in ['CNRM','KNMI','HCLIMcom','MOHC','ICTP']:
            rlat,rlon,pole = ds_temp.rlat, ds_temp.rlon, ds_temp.rotated_pole
            pole_lon = pole.attrs['grid_north_pole_longitude']
            pole_lat = pole.attrs['grid_north_pole_latitude']
            crs = ccrs.RotatedPole(pole_longitude=pole_lon,
                                    pole_latitude=pole_lat)
        elif cpm == 'MOHC':
            rlat,rlon,pole = ds_temp.rlat, ds_temp.rlon, ds_temp.rotated_latitude_longitude
            pole_lon = pole.attrs['grid_north_pole_longitude']
            pole_lat = pole.attrs['grid_north_pole_latitude']

            crs = ccrs.RotatedPole(pole_longitude=pole_lon, pole_latitude=pole_lat)

        else:
            rlat,rlon = ds_temp.lat, ds_temp.lon

        ### Set map extent
        map_ext = np.array([-180, 180, -90, 90])  # [degree]
        # map_ext = np.array([-0, 11, 45, 49])  # [degree]
        # map_ext = np.array([5.2, 11.4, 45, 48.3])  # [degree]
        rad_earth = 6371.0  # approximate radius of Earth [km]
        dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(
            map_ext[2:].mean()))) / 360.0 * (map_ext[1] - map_ext[0])
        dist_y = 2.0 * np.pi* rad_earth / 360.0 * (map_ext[3] - map_ext[2])

        dist_y, dist_x = 1,1
        ### ---------------------- Change-Signal ---------------------- ###
        ### Create figure that is adjusted for latitudinal distortion
        fig, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
                            subplot_kw={'projection':ccrs.PlateCarree()})

        ax.coastlines(color='black', linewidth=0.5)
        ax.add_feature(cfeature.BORDERS, edgecolor='black', linewidth=0.5)
        ax.set_aspect("auto")
        # ax.set_extent(map_ext, crs=ccrs.PlateCarree())
        norm = mpl.colors.TwoSlopeNorm(vmin=-100., vcenter=0., vmax=100)
        cmap = ListedColormap(cm.BrBG(np.linspace(0,1,20)))

        if cpm not in ['CNRM','KNMI','HCLIMcom','ICTP']:
            col = ax.pcolormesh(rlon, rlat, tas_ensemble, cmap=cmap, norm=norm,
                        transform=crs, rasterized=True)

        else:
            col = ax.pcolormesh(rlon, rlat, tas_ensemble, cmap=cmap,
                                    norm=norm, rasterized=True)

        cbar_ax= fig.add_axes([0.92,0.122,0.03,.75])
        minorticks = np.arange(-100, 101, 10)
        majorticks = np.arange(-100, 101, 20)
        cbar = mpl.colorbar.ColorbarBase(cbar_ax, cmap=cmap,
                                norm=norm,
                                extend='both',
                                ticks=majorticks,
                                spacing='proportional',
                                orientation='vertical')
        cbar.ax.tick_params(labelsize=10)
        cbar.ax.yaxis.set_ticks(minorticks, minor=True)

        # if cpm == 'EnsembleMean':
        #     plt.suptitle((f'Change in daily mean temperture at {title_name[scenario]} '
        #                 +f' \n' 
        #                 +f'{season_names[season]} |'
        #                 +' Ensemble Mean'), y=0.95, fontsize=17)
        # else:
        #     plt.suptitle((f'Change in daily mean temperature at {title_name[scenario]} '
        #             +f' \n' 
        #             +f'{season_names[season]} |'
        #             +f' {cpm}'), y=0.95, fontsize=17)
        plt.savefig(os.path.join(plot_path, 
                    f'TasChange_{scenario[-3:]}_{season}.pdf'),
                    format='pdf',bbox_inches='tight')
        plt.show()
        breakpoint()
        # plt.close()



        # if cpm in Institutes_tas:
        #     ### ---------------------- tas ---------------------- ###
        #     ### Calculate where Claus.-Clapeyron relation is seen (==7%/°C)
        #     CC_ensemble = np.where((tas_ensemble >= 6.5) & (tas_ensemble < 7.5),
        #                             tas_ensemble, np.nan)
        #     rlat,rlon = ds_temp.lat, ds_temp.lon


        #     ### Create figure that is adjusted for latitudinal distortion
        #     fig2, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
        #                         subplot_kw={'projection':ccrs.PlateCarree()})

        #     ax.coastlines(color='black', linewidth=0.5)
        #     ax.add_feature(cfeature.BORDERS, edgecolor='black', linewidth=0.5)
        #     ax.set_aspect("auto")
        #     ax.set_extent(map_ext, crs=ccrs.PlateCarree())
        #     # norm2 = mpl.colors.TwoSlopeNorm(vmin=-10., vcenter=0., vmax=45)
        #     # norm2 = mpl.colors.BoundaryNorm([-10,-5,0,5,10,15,20,25,30,35,40,45], 12)
        #     norm2 = mpl.colors.BoundaryNorm(np.linspace(-8,44,14), 14)
        #     # breakpoint()
        #     col_a = (cm.BrBG(np.linspace(0.2,0.45,3))).tolist()[:2]
        #     col_b = (cm.BrBG(np.linspace(0.5,1,11))).tolist()
        #     # col_c = (cm.BrBG(np.linspace(0.5,0.5,1))).tolist()
        #     # col_a.extend(col_c)
        #     col_a.extend(col_b)
        #     col_array = np.array(col_a)
        #     cmap2 = ListedColormap(col_array)
        #     # ax.contourf(rlon, rlat, tas_ensemble,100,rasterized=True)
        #     col2 = ax.pcolormesh(rlon, rlat, tas_ensemble, cmap=cmap2,
        #                             norm=norm2, rasterized=True)

            
        #     # col2 = ax.contourf(rlon, rlat, tas_ensemble, cmap=cmap2,
        #     #                     norm=norm2)
        #     # ax.contourf(rlon, rlat, CC_ensemble, colors='black')

        #     cbar_ax2 = fig2.add_axes([0.92,0.122,0.03,.75])
        #     minorticks = np.arange(-8, 44, 4)
        #     majorticks = np.arange(-8, 44, 8)
        #     cbar2 = mpl.colorbar.ColorbarBase(cbar_ax2, 
        #                             cmap=cmap2,
        #                             norm=norm2,
        #                             extend='both',
        #                             ticks=majorticks,
        #                             spacing='proportional',
        #                             orientation='vertical')
        #     # cbar2.ax.set_yscale('linear')
        #     cbar2.ax.tick_params(labelsize=10)
        #     cbar2.ax.yaxis.set_ticks(minorticks, minor=True)
            

        #     if cpm == 'EnsembleMean':
        #         plt.suptitle((f'tas of max. 60min precip. at {title_name[scenario]} '
        #                     +f'for a {rp}-year return period [%], \n' 
        #                     +f'{season_names[season]} |'
        #                     +' Ensemble Mean'), y=0.95, fontsize=17)
        #     else:
        #         plt.suptitle((f'tas of max. 60min precip. at {title_name[scenario]} '
        #                 +f'for a {rp}-year return period [%], \n' 
        #                 +f'{season_names[season]} |'
        #                 +f' {cpm}'), y=0.95, fontsize=17)
                
        #     plt.savefig(os.path.join(plot_path, 
        #                 f'tas_{scenario[-3:]}_{season}_RP{rp}.pdf'),
        #                 format='pdf',bbox_inches='tight')
        #     # plt.show()
        #     # breakpoint()
        #     # plt.close()
