#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
January 2024

Authors:
- Leandro Gimmi

Description:
Plot GEV values for different seasons and scenarios and models

"""
import os
import numpy as np
import xarray as xr
import pandas as pd
import matplotlib as mpl
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
import cartopy.feature as cfeature
from matplotlib.ticker import ScalarFormatter
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.patches as mpatches
import warnings
warnings.simplefilter("ignore")


'''-------------------------------------------------------------------------'''

### Choose between  historical
scenario = 'historical'

### xHourly, xDaily
time_res = ['1hr','3hr','6hr','1d','3d','5d']

### Seasons: DJF, MAM, JJA, SON
seasons = ['JJA', 'SON', 'DJF', 'MAM']

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)
# return_periods = np.insert(return_periods,0,2)
# return_periods = np.append(return_periods, [150, 200, 250, 300])
rp_index = np.arange(2,201,1)

'''-------------------------------------------------------------------------'''
### ------------------------------- CPMs ----------------------------------- ###
plot_path = f'/users/lgimmi/MeteoSwiss/figures/GEV_individual/{scenario}/'

path_region = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'

Institutes = ['stations','ensemble','CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH',
              'CLMcom-JLU', 'CLMcom-KIT','CNRM','KNMI','HCLIMcom']
# Institutes = ['stations','ensemble']

region_numbers = {"CH":1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1,
                   "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                 "SON": 'Autumn'}

'''-------------------------------------------------------------------------'''

for region in region_numbers.keys():
    ### Open Figure
    fig = plt.figure(figsize=(20, 10))
    gs = fig.add_gridspec(nrows=4, ncols=6)
    row, column = 0, 0
    print((f'Region: {region}').center(60,'~'))
    for season in seasons:
        print((f'Seasons: {season}').center(1,' '))
        for tres in time_res:
            ax = fig.add_subplot(gs[row,column])
            if tres in ['1hr','1d']:
                divide = 1
            elif tres in ['3hr','3d']:
                divide = 3
            elif tres in ['6hr']:
                divide = 6
            elif tres in ['5d']:
                divide = 5
            print((f'Time resolution: {tres}').center(40,' '))
            for cpm in Institutes:
                
                path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{tres}/pr_GEV/'
                            +f'{scenario}')

                ### Load GEV values
                if cpm != 'stations':
                    if cpm == 'ensemble':
                        # ds = xr.open_dataset(path_data +
                        #     f'/OneYearStep_EnsembleMean_{tres}_RP_prmax_{season}.nc')
                        ds = xr.open_dataset(path_data +
                                f'/OneYearStep_EnsembleMedian_{tres}_RP_prmax_{season}.nc')
                    else:
                        ds = xr.open_dataset(path_data +
                            f'/OneYearStep_{cpm}_{tres}_RP_prmax_{season}.nc')
                    ds_region = ds[region]
                    
                    lmean, lmedian, ltenP, lninetyP = [], [], [], []
                    for rp in return_periods:
                        rpidx = np.where(rp_index == rp)[0][0]
                        vmean = ds_region[rpidx][0]
                        vmedian = ds_region[rpidx][1]
                        vtenP = ds_region[rpidx][2]
                        vninetyP = ds_region[rpidx][3]

                        ### Use divide to get hourly values
                        lmean.append(np.nanmean(vmean) / divide)
                        lmedian.append(np.nanmean(vmedian) / divide)
                        ltenP.append(np.nanmean(vtenP) / divide)
                        lninetyP.append(np.nanmean(vninetyP) / divide)

                elif cpm == 'stations':
                    path_stations = '/users/lgimmi/MeteoSwiss/data' 
                    path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'

                    if tres in ['1hr','3hr','6hr']:
                        fname_metadata = (path_stations + '/station_metadat_rre150h0_1995_2010_'
                                            +'allstats_andpartnerstats_withNA.csv')
                        region_data = pd.read_csv(path_region_stations + 
                            f'/stations_region_{scenario}_1hr.csv')
                    elif tres in ['1d','3d','5d']:
                        fname_metadata = (path_stations + '/station_metadat_rre150d0_1995_'
                                            +'2010_allstatswithpartnerstats_allNA.csv')
                        region_data = pd.read_csv(path_region_stations + 
                            f'/stations_region_{scenario}_1d.csv')
                        
                    metadata = pd.read_csv(fname_metadata)
                    lon = metadata['longitude']
                    lat = metadata['latitude']
                    

                    data = np.load(path_data + 
                        f'/OneYearStep_Values_{cpm}_{season}_{scenario}_{tres}.npy',
                         allow_pickle=True)
                    mrp, lrp, trp, nrp = [], [], [], []
                    # if cpm == 'stations' and tres == '1d':
                    #     breakpoint()
                    for i_rp, rp in  enumerate(return_periods):
                        meandata, stat_data, tendata, nindata = [], [], [], []
                        ### Loop over stations
                        for i_stat, stat_name in enumerate(data.keys()):
                            if stat_name not in region_data.keys():
                                continue
                            elif region_data[stat_name][0] != region and region != 'CH':
                                continue
                            meandata.append(data[stat_name][rp][0]) ### Mean of all Bootstraps (0)
                            stat_data.append(data[stat_name][rp][1]) ### Median of all Bootstraps (1)
                            tendata.append(data[stat_name][rp][2]) ### 10th percentile of all Bootstraps (2)
                            nindata.append(data[stat_name][rp][3]) ### 90th percentile of all Bootstraps (3)
                            
                        ### Use divide to get hourly/daily values
                        mrp.append(np.nanmean(meandata) / divide )
                        lrp.append(np.nanmean(stat_data) / divide )     
                        trp.append(np.nanmean(tendata) / divide )
                        nrp.append(np.nanmean(nindata) / divide )                

                ### Plot
                if cpm == 'ensemble':
                    ax.plot(return_periods, lmedian, label='Ensemble Median',
                        color='dodgerblue', linestyle='-')
                elif cpm == 'stations':
                    ax.plot(return_periods, lrp, label='Stations',
                        color='black', linestyle='-')
                    ax.plot(return_periods, trp, label='10th percentile',
                        color='black', linestyle='--', linewidth=0.7,
                        zorder=-10)
                    ax.plot(return_periods, nrp, label='90th percentile',
                        color='black', linestyle='--', linewidth=0.7,
                        zorder=-10)
                else:
                    ax.plot(return_periods, lmedian, label=f'{cpm}',
                        color='dodgerblue', linestyle='-', alpha=0.3,
                        linewidth=0.5)
           
            ### Plot settings
            def setmaxyval():
                global max_y
                if tres == '1hr' and season == 'DJF':
                    max_y = 20
                elif tres == '1hr' and season == 'MAM':
                    max_y = 60
                elif tres == '1hr' and season == 'JJA':
                    max_y = 60
                elif tres == '1hr' and season == 'SON':
                    max_y = 60

                elif tres == '3hr' and season == 'DJF':
                    max_y = 20
                elif tres == '3hr' and season == 'MAM':
                    max_y = 30
                elif tres == '3hr' and season == 'JJA':
                    max_y = 30
                elif tres == '3hr' and season == 'SON':
                    max_y = 30

                elif tres == '6hr' and season == 'DJF':
                    max_y = 20
                elif tres == '6hr' and season == 'MAM':
                    max_y = 20
                elif tres == '6hr' and season == 'JJA':
                    max_y = 20
                elif tres == '6hr' and season == 'SON':
                    max_y = 20

                elif tres == '1d' and season == 'DJF':
                    max_y = 100
                elif tres == '1d' and season == 'MAM':
                    max_y = 100
                elif tres == '1d' and season == 'JJA':
                    max_y = 160
                elif tres == '1d' and season == 'SON':
                    max_y = 160

                elif tres == '3d' and season == 'DJF':
                    max_y = 75
                elif tres == '3d' and season == 'MAM':
                    max_y = 75
                elif tres == '3d' and season == 'JJA':
                    max_y = 75
                elif tres == '3d' and season == 'SON':
                    max_y = 75

                elif tres == '5d' and season == 'DJF':
                    max_y = 60
                elif tres == '5d' and season == 'MAM':
                    max_y = 60
                elif tres == '5d' and season == 'JJA':
                    max_y = 60
                elif tres == '5d' and season == 'SON':
                    max_y = 60

            setmaxyval()

            # ax.set_facecolor('whitesmoke')
            if row == 0:
                ax.set_title(f'{tres}',fontsize=15)
            if column == 0:
                props = dict(facecolor='white', alpha=0.5)
                ax.text(-0.4,0.32,f'{season_names[season]}',rotation=90,
                    transform=ax.transAxes, fontsize=15)

            major_ticks_y = np.arange(0, max_y+1, max_y/10*2)
            minor_ticks_y = np.arange(0, max_y+1, max_y/10)
            if tres in ['1hr','3hr','6hr']:
                ax.set_ylabel('Intensity [mm/h]')
            elif tres in ['1d','3d','5d']:
                ax.set_ylabel('Intensity [mm/d]')
            
            ax.set_yticks(major_ticks_y)
            ax.set_yticks(minor_ticks_y, minor=True)
            ax.set_ylim(0,max_y)
            ax.grid(which='both', axis='y', alpha=0.3)

            ax.set_xscale('log')
            if row == 3:
                ax.set_xlabel('Returnperiod [yr]')
                
            # else:
            #     ax.set_xticks([])
            ax.set_xticks([5,10,20,50,100])
            ax.get_xaxis().set_major_formatter(ScalarFormatter())
            plt.xlim(return_periods[0],return_periods[-1])
            plt.suptitle(f'Ensemble median | Region: {region} | Time period: historical',
                          fontsize=20)
                

            if column == 5:
                column = 0
                row += 1
            else:
                column += 1

    # plt.show()
    # breakpoint()
    plt.subplots_adjust( wspace=0.4, hspace=0.4)
    plt.savefig(os.path.join(plot_path, f'Overview_EnsembleMedian_{region}.pdf'),
                        format='pdf',bbox_inches='tight')
    # plt.savefig(os.path.join(plot_path, f'Overview_EnsembleMean_{region}.pdf'),
    #                     format='pdf',bbox_inches='tight')
    plt.close()


