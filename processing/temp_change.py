# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
September 2023

Authors:
- Leandro Gimmi

Description:
Calculate grid cell wise temperature change for individual Convective 
Permitting Models (CPM) or conventional regional climate models (RCM) which 
can be then used for temperature scaling of precipitation changes.

"""
import os
import copy
import glob
import time
import warnings
import xesmf as xe
import numpy as np
import xarray as xr 
import matplotlib as mpl
import cartopy.crs as ccrs
from natsort import natsorted
import matplotlib.pyplot as plt
from multiprocessing import Pool
from matplotlib.pyplot import cm
import cartopy.feature as cfeature
from matplotlib.colors import LinearSegmentedColormap, ListedColormap
warnings.simplefilter("ignore")

'''-------------------------------------------------------------------------'''
### rcp85_moc / rcp85_eoc | Scenario that you are interested in
scenario = 'rcp85_eoc'

### 1d as we used daily mean temperature for the scaling regardeles of the time
### resolution of the presipitation data
time_res = '1d'

### DJF, MAM, JJA, SON | Season(s) that you are interested in
seasons = ['DJF', 'MAM', 'SON', 'JJA']
# seasons = ['JJA']

### CHNE, CHW, CHS, CHAE or CHAW | CH2018 region(s) that you are interested in
regions = ['CH','CHNE', 'CHW', 'CHS', 'CHAE', 'CHAW']

### Set your data and save paths here
### ------------------------------- CPM ----------------------------------- ###
## Path to scenario precipitation data
path_prdata = f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/tas/{scenario}'
## Path to historical precipitation data
h_path = f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/tas/historical'
## Path to CH2018 mask files
path_regions = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
## Path to save temperature change figures (if neeeded)
plot_path = f'/users/lgimmi/MeteoSwiss/figures/day/maps/'
## Path to save temperature change data
path_save = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/tas/cc_signal')

## List of CPMs that should be considered. The first one will be used as target 
## for regridding of all other models
Institutes = ['CLMcom-ETH','CLMcom-BTU','CLMcom-CMCC','CLMcom-JLU',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom','MOHC','ICTP']
CPMs = ['COSMO-crCLIM','CCLM5-0-14','CCLM5-0-9','CCLM5-0-15',
        'CCLM5-0-15','AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME',
        'HadREM3-RA-UM10.1','RegCM4-7']
# Institutes = ['CLMcom-ETH','CLMcom-BTU','CLMcom-CMCC','CLMcom-JLU',
#               'CLMcom-KIT','CNRM','KNMI','HCLIMcom']
# CPMs = ['COSMO-crCLIM','CCLM5-0-14','CCLM5-0-9','CCLM5-0-15',
#         'CCLM5-0-15','AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME']

rcm = None

### ------------------------------- RCM ----------------------------------- ###
# path_prdata = f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/tas/{scenario}'
# h_path = f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/tas/historical'
# path_regions = '/scratch/snx3000/lgimmi/store/rcm/masks'
# plot_path = '/users/lgimmi/MeteoSwiss/figures/day/maps/tas/'
# path_save = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/tas/cc_signal')

# Institutes = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom']
# CPMs = ['COSMO-crCLIM','ALADIN63','RACMO23E','HCLIM38-ALADIN']

# rcm = 1

'''-------------------------------------------------------------------------'''
if scenario == 'rcp85_moc':
    start_year = 2040
    path_prdata = path_prdata[:-4] + '/moc'
    path_save = path_save + '/moc'
    plot_path = plot_path + '/moc'
elif scenario == 'rcp85_eoc':
    start_year = 2090
    path_prdata = path_prdata[:-4] + '/eoc'
    path_save = path_save + '/eoc'
    plot_path = plot_path + '/eoc'
title_name = {'rcp85_eoc': 'end of century',
              'rcp85_moc': 'mid of century'}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                "SON": 'Autumn'}

def fill_patches_with_NNmean(arr, patch_size=5, dimension='1D'):
    '''
    Fixes patches created by regridding of models
    '''
    from scipy.interpolate import griddata

    filled_arr = np.copy(arr)

    # Find nan to values transitions which are not at the border, i.e.
    # patches to be filled
    if dimension == '1D':
        patches = \
            np.argwhere(np.isnan(arr) & 
                        ~np.isnan(np.roll(arr,patch_size)) &
                        ~np.isnan(np.roll(arr,-patch_size)))

    elif dimension == '2D':
        patches = \
            np.argwhere(np.isnan(arr) & 
                        ~np.isnan(np.roll(arr,[patch_size, patch_size],
                                            axis=(0, 1))) &
                        ~np.isnan(np.roll(arr,[-patch_size, -patch_size], 
                                            axis=(0, 1))))
    if len(patches) != 0:
        # Interpolate using griddata where we have patches
        filled_arr[tuple(zip(*patches))] = \
                griddata(np.array(np.where(~np.isnan(arr))).T,
                        arr[~np.isnan(arr)],
                        tuple(patches.T),
                        method='nearest')
    
    return filled_arr

# def get_gridinfo():
# target_file = ('/scratch/snx3000/lgimmi/store/ALP-3/1hr/pr/historical/'
#                 +'pr_ALP-3_MPI-M-MPI-ESM-LR_historical_r1i1p1_CLMcom-ETH-COSMO-crCLIM_fpsconv-x2yn2-v1_1hr_19960101-19961231.nc')
# modload = ('module load daint-gpu ncview')
# modloadCDO = ('module load CDO')
# get_grid = ('cdo griddes ' + target_file +
#                 ' > /users/lgimmi/MeteoSwiss/griddes/'
#             +f'griddes_ETHpr.txt')
# os.system(modload)
# os.system(modloadCDO)
# os.system(get_grid)
# get_gridinfo()

for season in seasons:
    fig2 = plt.figure(figsize=(15, 12))
    gs = fig2.add_gridspec(nrows=3, ncols=3)
    row = 0
    column = 0
    print((f'Season: {season}').center(50,'-'))
    for i_cpm, cpm in enumerate(Institutes):
        onlyfirst = 0
        print((f' Model: {cpm}').center(60,'~'))
        for i_regio, region in enumerate(regions):
            i_regio+=1
            ### Assign number found in mask-file to region
            CHNE, CHW, CHS ,CHAE, CHAW, CH = 1, 2, 3, 4, 5, 1
            myVars = locals()
            i_region = myVars[region]
         
            print(((f'Region: {region}').center(50,' ')))
        
            if cpm == 'KNMI':
                filename = (path_prdata + '/' + f'*{CPMs[i_cpm]}*')
                h_fname = (h_path + '/' + f'*{CPMs[i_cpm]}*')
            elif cpm == 'MOHC':
                filename = (path_prdata + '/' + f'*{CPMs[i_cpm]}*')
                h_fname = (h_path + '/' + f'*{CPMs[i_cpm]}*')
            else:
                filename = (path_prdata + '/' + f'*{cpm}**{CPMs[i_cpm]}*')
                h_fname = (h_path + '/' + f'*{cpm}**{CPMs[i_cpm]}*')
            filenames_list = natsorted(glob.glob(filename))
            h_fname_list = natsorted(glob.glob(h_fname))
            ### Open historical and future data
            ds = xr.open_mfdataset(filenames_list,use_cftime=True)
            h_ds = xr.open_mfdataset(h_fname_list,use_cftime=True)
            ds = ds.groupby("time.season")[season]
            h_ds = h_ds.groupby("time.season")[season]
            # da, h_da = ds.tas, h_ds.tas
            da, h_da = ds, h_ds
            ds_new = copy.deepcopy(ds)

            # da = da.sel(time=da.time[:])
            # h_da = h_da.sel(time=h_da.time[:])

            # if i_cpm == 0:
            #     target_grid, target_name = copy.deepcopy(da), cpm
            #     h_target_grid, h_target_name = copy.deepcopy(da), cpm
            # else:
            #     regridder = xe.Regridder(da, target_grid, "bilinear")
            #     h_regridder = xe.Regridder(h_da, h_target_grid, "bilinear")
            #     print(f'Regridding {cpm} to {target_name} for multimodel mean')
            #     da = regridder(da)
            #     h_da = h_regridder(h_da)

            ### open mask file for regions
            if region != 'CH':
                endname = 'Ch2018'
                var_name = 'orog'
            else:
                endname = 'maskCH'
                var_name = 'mask_CH'
            if cpm == 'MOHC':
                region_file = (path_regions + f'/*{CPMs[i_cpm]}*{endname}*')
            # elif rcm == 1 and cpm == 'CNRM':
            #     region_file = (path_regions + 
            #                     f'/*{cpm}*{CPMs[i_cpm]}*Ch2018*')
            else:
                region_file = (path_regions + 
                                f'/*{cpm}*{CPMs[i_cpm]}*{endname}*')
            ds_region = xr.open_mfdataset(region_file)
            mask_region = ds_region[var_name]
            ds_region.close()
            ### Allign masks to dimensions and cooridinates of CPM as they
            ###  differ slightly on the 7th or higher decimal place
            mask_region = \
                mask_region.expand_dims(dim={"time": len(da.time)})
            mask_region = mask_region.assign_coords(time=da.time)
            da, mask_region = xr.align(da, mask_region, join="override")
            h_mask_region = ds_region[var_name]
            h_mask_region = \
                h_mask_region.expand_dims(dim={"time": len(h_da.time)})
            h_mask_region = h_mask_region.assign_coords(time=h_da.time)
            h_da, h_mask_region = xr.align(h_da, h_mask_region, join="override")

            if cpm == 'CLMcom-CMCC':
                ### All of CH Mask file of CMCC is wrongly labeled
                try:
                    mask_region = mask_region.rename({'x':'rlon','y':'rlat'})
                    mask_region['rlon'] = ds['pr']['rlon']
                    mask_region['rlat'] = ds['pr']['rlat']
                except:
                    pass
                try:
                    h_mask_region = h_mask_region.rename({'x':'rlon','y':'rlat'})
                    h_mask_region['rlon'] = h_ds['pr']['rlon']
                    h_mask_region['rlat'] = h_ds['pr']['rlat']
                except:
                    pass
            
            # # '''-----------------------------------------------------'''
            # ### Diff. way to mask data (extent outside region is kept)
            # ### Separate masking into blocks of size limited to 1.0GB
            # ### to avoid freezing of code due to memory issues
            # ### Used for CMCC model as it has coordinate problem
            # # '''-----------------------------------------------------'''

            def masking(xarray_dataarray,  mask, max_block = 1.0):
                '''
                Separate masking into blocks of size limited to 1.0GB 
                (max_block) to avoid freezing of code due to memory issues

                -----
                Returns maked input array as numpy array

                '''
                global i_region

                block_size = max_block
                da = xarray_dataarray
                mask_region = mask
                block_size = 1.0

                len_time = da['time'].size
                if ('x' in list(da.coords)) and ('y' in list(da.coords)):
                    len_x = da['x'].size
                    len_y = da['y'].size
                    out_dim = {'y','x'}
                elif ('rlon' in list(da.coords)) and ('rlat' in list(da.coords)):
                    len_x = da['rlon'].size
                    len_y = da['rlat'].size
                    out_dim = {'rlat','rlon'}
                elif ('lon' in list(da.coords)) and ('lat' in list(da.coords)):
                    len_x = da['lat'].size
                    len_y = da['lon'].size
                    out_dim = {'lon','lat'}
                else:
                    print(f'The model {cpm} uses an unknown coordinate systems'
                        +f' -- {list(da.coords)} -- ')
                if cpm == 'CLMcom-CMCC':
                    len_x = ds['rlon'].size
                    len_y = ds['rlat'].size
                    out_dim = {'rlat','rlon'}

                dataarray = np.empty((len_time, len_y, len_x),dtype=np.float32)
                dataarray.fill(np.nan)
                maskarray = np.empty((len_time, len_y, len_x),dtype=np.float32)
                maskarray.fill(np.nan)
                da_masked = np.empty((len_time, len_y, len_x),dtype=np.float32)
                da_masked.fill(np.nan)

                num_blocks = \
                    int(np.ceil((da_masked.nbytes/ (10 ** 9))/block_size))
                lim = np.linspace(0, len_time, num_blocks + 1, dtype=np.int32)  
                
                for i in range(num_blocks):
                    t_beg = time.time()
                    slice_t = slice(lim[i], lim[i + 1])
                    dataarray[slice_t,:,:] = da[slice_t,:,:].values
                    maskarray[slice_t,:,:] = mask_region[slice_t,:,:].values

                    da_masked[slice_t,:,:] = np.where(
                        maskarray[slice_t,:,:] == float(i_region),
                        dataarray[slice_t,:,:], #change units to mm/hr
                        0.0)
                    
                    print('Data blocks loaded: '+str(i+ 1)+'/'+str(num_blocks))
                    print('Time for masking for this block: ' + 
                        '%.2f' % (time.time() - t_beg) + ' s')
                return da_masked

            # '''-------------------------------------------------------'''
            ### Masking using xarray can drop unnecessairy grid cells to 
            ### speed up code and free up memory (Not working with CMCC)
            # '''-------------------------------------------------------'''   

            ### Regrid data to ETH grid for all members of Ensemble
            ds_masked_nc = da.where(mask_region == i_region)
            h_ds_masked_nc = h_da.where(h_mask_region == i_region)
            tas_mean_nc = ds_masked_nc['tas'].mean('time')
            h_tas_mean_nc = h_ds_masked_nc['tas'].mean('time')
            
            try:
                    ds_new = ds_new.drop('time_bnds')
                    ds_masked_nc = ds_masked_nc.drop('time_bnds')
            except:
                pass

            # ds_new = ds_masked_nc.copy(deep=True)
            ds_new = ds_new.sel(time=ds_new.time[0])
            ds_new['tas_mean'] = (tas_mean_nc - h_tas_mean_nc)
            proc_info = ('Change in daily temperature')
            ds_new.attrs["processing_information"] = proc_info
            ds_new = ds_new.drop('tas')
            attrs_info = ds_new.attrs
            # ds_new = ds_new.mean('time')
            ds_new.attrs = attrs_info

            if cpm in ['ICTP','CNRM','KNMI']:
                ### These models are missing some coordinate units information
                try:
                    ds_new['x'].attrs['units'] = 'degrees north'
                    ds_new['y'].attrs['units'] = 'degrees east'
                except:
                    pass
                
            ### Add regions together for plottings
            if region != 'CH':
                if onlyfirst == 0:
                    mapvalues = ds_new['tas_mean'].values
                    mapvalues = np.where(mapvalues == np.nan, 0, mapvalues)
                    onlyfirst += 1
                else:
                    mapvalues = np.where(np.isnan(mapvalues) == True,
                                    ds_new['tas_mean'].values, mapvalues)
                    
            ### ----------------------------------------------------------- ###
            ### Save and regridd files, they are regrided to the first CPM in list (ETH)

            if i_cpm == 0:
                try:
                    rm_old_file = ('rm ' + path_save + 
                        f'/Regridded_ccsignal_{cpm}_{region}_{season}.nc')
                    os.system(rm_old_file)
                except:
                    print('No old file to remove')
                filename_nc = (path_save + 
                            f'/Regridded_ccsignal_{cpm}_{region}_{season}.nc')
                target_file, target_name = copy.deepcopy(filename_nc), cpm
                
                lonlatfilepath = ('/scratch/snx3000/lgimmi/store/ALP-3/1hr/pr/historical/'
                +'pr_ALP-3_MPI-M-MPI-ESM-LR_historical_r1i1p1_CLMcom-ETH-COSMO-crCLIM_fpsconv-x2yn2-v1_1hr_19960101-19961231.nc')
                lonlatfile = xr.open_dataset(lonlatfilepath)
                # ds_new = ds_new.assign_coords(lon=lonlatfile.coords['lon'],
                #                            lat=lonlatfile.coords['lat'])
                
                target_grid = copy.deepcopy(ds_new)
                ds_new.to_netcdf(path=filename_nc)
                ds_new.close()

                # modload = ('module load daint-gpu ncview')
                # modloadCDO = ('module load CDO')
                # get_grid = ('cdo griddes ' + target_file +
                #              ' > /users/lgimmi/MeteoSwiss/griddes/'
                #             +f'griddes_ETHpr_{region}.txt')
                # os.system(modload)
                # os.system(modloadCDO)
                # os.system(get_grid)
                
            else:
                # lonlatfilepath = ('/scratch/snx3000/lgimmi/store/ALP-3/1hr/pr/historical/'
                # +'pr_ALP-3_MPI-M-MPI-ESM-LR_historical_r1i1p1_CLMcom-ETH-COSMO-crCLIM_fpsconv-x2yn2-v1_1hr_19960101-19961231.nc')
                # lonlatfile = xr.open_dataset(lonlatfilepath)
                # breakpoint()
                # ds_new = ds_new.assign_coords(lon=lonlatfile.coords['lon'],
                #                            lat=lonlatfile.coords['lat'])
                # breakpoint()
                regridder = xe.Regridder(ds_new, target_grid, "bilinear")
                ds_regridded = regridder(ds_new)
                ds_regridded =  ds_regridded.where(ds_regridded != 0, np.nan)
                filename_nc = (path_save + f'/Regridded_ccsignal_{cpm}_{region}_{season}.nc')
                ds_regridded.to_netcdf(path=filename_nc)

                # try:
                #     rm_old_file = ('rm ' + path_save + 
                #                 # f'/ccsignal_{cpm}_{region}_{season}.nc')
                #         f'/TobeRegridded_ccsignal_{cpm}_{region}_{season}.nc')
                #     os.system(rm_old_file)
                # except:
                #     print('No old file to remove')
                # filename_nc = (path_save + 
                #             #    f'/ccsignal_{cpm}_{region}_{season}.nc')
                #             f'/TobeRegridded_ccsignal_{cpm}_{region}_{season}.nc')
                # ds_new.to_netcdf(path=filename_nc)
                # ds_new.close()

                # try:
                #     rm_old_file = ('rm ' + path_save + 
                #                 # f'/ccsignal_{cpm}_{region}_{season}.nc')
                #         f'/Regridded_ccsignal_{cpm}_{region}_{season}.nc')
                #     os.system(rm_old_file)
                # except:
                #     print('No old file to remove')

                # ### Regrid data to ETH grid for all members of Ensemble
                # remapp_to_pr_ETH = ('cdo remapbil,'
                #     # +f'/users/lgimmi/MeteoSwiss/griddes/griddes_ETHpr_{region}.txt '
                #     +f'/users/lgimmi/MeteoSwiss/griddes/griddes_ETHpr.txt '
                #     +'/scratch/snx3000/lgimmi/store/ALP-3/1d/tas/cc_signal'
                #     +f'/{scenario[-3:]}/TobeRegridded_ccsignal_{cpm}_{region}_{season}.nc '
                #     +f'/scratch/snx3000/lgimmi/store/ALP-3/1d/tas/cc_signal/'
                #     +f'{scenario[-3:]}/Regridded_ccsignal_{cpm}_{region}_{season}.nc')
                # os.system(modload)
                # os.system(modloadCDO)
                # os.system(remapp_to_pr_ETH)

                # rm_old_file = ('rm ' + path_save + 
                #     f'/TobeRegridded_ccsignal_{cpm}_{region}_{season}.nc')
                # os.system(rm_old_file)
            ### ----------------------------------------------------------- ###
                
            ### UNCOMMENT if non-nc file is wanted
            t_beg = time.time()
            da, h_da = da.tas, h_da.tas
            if cpm == 'CLMcom-CMCC':
                da_masked = masking(da, mask_region, max_block = 1.0)
                h_da_masked = masking(h_da, h_mask_region, max_block = 1.0)
            else:
                xrda_masked = da.where(mask_region == i_region,
                                        drop = True)
                da_masked = np.nan_to_num(xrda_masked.values)
                h_xrda_masked = h_da.where(h_mask_region == i_region,
                                        drop = True)
                h_da_masked = np.nan_to_num(h_xrda_masked.values)
                print('Time for masking and changing units: ' + 
                        '%.2f' % (time.time() - t_beg) + ' s')

            h_temp = np.nanmean(h_da_masked, axis=0)
            temp = np.nanmean(da_masked, axis=0)

            temp_change = (temp - h_temp)
            

            try:
                rm_old_file = ('rm ' + path_save + 
                               f'/ccsignal_{cpm}_{region}_{season}.npy')
                os.system(rm_old_file)
            except:
                print('No old file to remove')

            np.save(path_save + '/' + f'ccsignal_{cpm}_{region}_{season}.npy',
                    np.array(temp_change, dtype=object), allow_pickle=True)
            
            ds.close()
            h_ds.close()
            ds_region.close()
            

            # '''-------------------------------------------------------'''
            ### Quick plot to check if region masking worked
            # '''-------------------------------------------------------'''
            # lat = da.lat.where(mask_region[0] == i_region, drop = True)
            # lon = da.lon.where(mask_region[0] == i_region, drop = True)
            # # map_ext = np.array([5.85, 10.65, 45.7, 47.9])  # [degree]
            # map_ext = np.array([5.8, 10.7, 45.6, 48.0])  # [degree]
            # rad_earth = 6371.0  # approximate radius of Earth [km]
            # dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(
            #     map_ext[2:].mean()))) / 360.0 * (map_ext[1] - map_ext[0])
            # dist_y = 2.0 * np.pi* rad_earth / 360.0 * (map_ext[3] - map_ext[2])

            # fig, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
            #                     subplot_kw={'projection':ccrs.PlateCarree()})
            # ax.coastlines(color='grey', linewidth=0.5)
            # ax.add_feature(cfeature.BORDERS, edgecolor= 'grey',linewidth = 0.5)
            # ax.set_aspect("auto")
            # ax.set_extent(map_ext, crs=ccrs.PlateCarree())
            # ax.contourf(lon, lat, h_xrda_masked[0], 
            #     cmap='terrain', transform= ccrs.PlateCarree())
            # plt.savefig(os.path.join(plot_path, 'masktest_tas.pdf'),
            #                         format='pdf',bbox_inches='tight')
                    
        ### --------------------------------------------------------------- ###
        ### Plot for all regions together
    #     mapvalues = fill_patches_with_NNmean(mapvalues)
                    
    #     ### Set map extent
    #     map_ext = np.array([5.2, 11.4, 45, 48.3])  # [degree]
    #     rad_earth = 6371.0  # approximate radius of Earth [km]
    #     dist_x = 2.0 * np.pi * (rad_earth * np.cos(np.deg2rad(
    #         map_ext[2:].mean()))) / 360.0 * (map_ext[1] - map_ext[0])
    #     dist_y = 2.0 * np.pi* rad_earth / 360.0 * (map_ext[3] - map_ext[2])

    #     try:    
    #         if cpm not in ['CNRM','KNMI','HCLIMcom','MOHC','ICTP','CLMcom-ETH']:
    #             rlat,rlon,pole = ds.rlat, ds.rlon, ds.rotated_pole
    #             pole_lon = pole.attrs['grid_north_pole_longitude']
    #             pole_lat = pole.attrs['grid_north_pole_latitude']
    #             crs = ccrs.RotatedPole(pole_longitude=pole_lon,
    #                                 pole_latitude=pole_lat)
    #         elif region == 'CHAW' and cpm == 'MOHC' and season == 'DJF':
    #             rlat,rlon,pole = ds.rlat, ds.rlon, ds.rotated_latitude_longitude
    #             pole_lon = pole.attrs['grid_north_pole_longitude']
    #             pole_lat = pole.attrs['grid_north_pole_latitude']
    #             crs = ccrs.RotatedPole(pole_longitude=pole_lon,
    #                                 pole_latitude=pole_lat)
    #         else:
    #             rlat,rlon = ds.lat, ds.lon

    #     except:
    #         print(f'No rotated pole found. Model: {cpm}, Season: {season}')
    #         breakpoint()

    #     ### Create figure that is adjusted for latitudinal distortion
    #     # fig, ax = plt.subplots(figsize=(12.0 , 12.0 * (dist_y / dist_x)),
    #     #                     subplot_kw={'projection':ccrs.PlateCarree()})
    #     try:
    #         ax = fig2.add_subplot(gs[column,row], projection=ccrs.PlateCarree())
    #     except:
    #         breakpoint()

    #     ax.coastlines(color='black', linewidth=0.5)
    #     ax.add_feature(cfeature.BORDERS, edgecolor='black', linewidth=0.5)
    #     ax.set_aspect("auto")
    #     ax.set_extent(map_ext, crs=ccrs.PlateCarree())      
    #     norm = mpl.colors.BoundaryNorm(np.linspace(-1.5,6.5,9), 9)
    #     # norm = mpl.colors.TwoSlopeNorm(vmin=-1, vcenter=0., vmax=10)  

    #     ### remove frame of figure
    #     # fig2.patch.set_visible(False)
    #     # ax.axis('off')  
    #     white = [1.0,1.0,1.0,1.0]
    #     colneg = [0.1]
    #     colpos = np.linspace(0.6,1,6)
    #     colneg = np.append(colneg, colpos)
    #     color_array = cm.coolwarm(colneg)
    #     color_array = np.insert(color_array, 1, white ,axis=0)
    #     cmap = ListedColormap(color_array)

    #     if cpm not in ['CNRM','KNMI','HCLIMcom','ICTP','CLMcom-ETH']:
    #         col = ax.pcolormesh(rlon, rlat, mapvalues, cmap=cmap,
    #                         transform=crs, rasterized=True, norm=norm)
    #     else:
    #         col = ax.pcolormesh(rlon, rlat, mapvalues, cmap=cmap,
    #                          rasterized=True, norm=norm)
        
    #     ### Add text box with regional mean
    #     regional_mean = np.nanmean(mapvalues)
    #     props = dict(boxstyle='Square', facecolor='white', alpha=0.5)
    #     ax.text(0.025,0.93,f'{regional_mean:.2f}',
    #             transform=ax.transAxes, bbox=props, fontsize=11)
        
    #     ax.title.set_text(f'{cpm}')

        
    #     if row == 2:
    #         row = 0
    #         column += 1
    #     else:
    #         row += 1

    # ### Colorbar
    # cbar_ax= fig2.add_axes([0.92,0.122,0.03,.75]) # vertical
    # minorticks = np.arange(-1, 7, 1)
    # majorticks = np.arange(-1, 7, 1)
    # cbar = mpl.colorbar.ColorbarBase(cbar_ax, cmap=cmap,
    #                             extend='both',
    #                             norm=norm,
    #                             ticks=majorticks,
    #                             spacing='proportional',
    #                             orientation='vertical')
    # cbar.ax.tick_params(labelsize=10)
    # cbar.ax.yaxis.set_ticks(minorticks, minor=True)
    # cbar.ax.tick_params(size=0,which=u'both') # remove ticks
    # cbar.set_label('Temperature change [°C]', rotation = 90, size=12)

    # fig2.suptitle(f'Temperatur change until {title_name[scenario]} '
    #               +f'| {season_names[season]}',
    #                fontsize=16)
    # if rcm != 1:
    #     plt.savefig(os.path.join(plot_path, 
    #                         f'TasChange_{scenario}_{season}.pdf'),
    #                         format='pdf',bbox_inches='tight')
    # elif rcm == 1:
    #     plt.savefig(os.path.join(plot_path, 
    #                         f'RCM_TasChange_{scenario}_{season}.pdf'),
    #                         format='pdf',bbox_inches='tight')

    # plt.show()
    # breakpoint()
