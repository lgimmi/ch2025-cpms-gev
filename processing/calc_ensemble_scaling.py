#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
December 2023

Authors:
- Leandro Gimmi

Description:
Calculate ensemble mean scaling by calculating scaling for individual models
 first
"""

import copy
import numpy as np
import xarray as xr

'''-------------------------------------------------------------------------'''
###-------------------------------------------------------------------------###
### Make sure to change names of the files to be read in and to be saved to 
### either OneYearStep or 'normal' (uneven steps but until 300y) files
###-------------------------------------------------------------------------###

### Choose between rcp85_moc / rcp85_eoc
scenario = 'rcp85_eoc'

### xHourly, xDaily
time_res = '1d'

### Daily data only in one year steps
### - - - - - - - - - - - - - - - -  CPM  - - - - - - - - - - - - - - - - - - -
path_pr = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario[:-4]}/{scenario[-3:]}')
path_pr_hist = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +'historical')
path_tas = (f'/scratch/snx3000/lgimmi/store/ALP-3/1d/tas/cc_signal/'
                +f'{scenario[-3:]}')

regions = {"CH": 1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1, "CHS": 3}
season_names = { "JJA": 'Summer',"DJF": 'Winter', "MAM": 'Spring',
                "SON": 'Autumn'}

Institutes = ['CLMcom-ETH','CLMcom-BTU','CLMcom-CMCC',
              'CLMcom-JLU', 'CLMcom-KIT','CNRM','KNMI','HCLIMcom']

### - - - - - - - - - - - - - - - -  RCM  - - - - - - - - - - - - - - - - - - -
# path_pr = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
#              +f'{scenario[:-4]}/{scenario[-3:]}')
# path_pr_hist = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
#              +'historical')
# path_tas = (f'/scratch/snx3000/lgimmi/store/rcm/1d/tas/cc_signal/'
#             +f'{scenario[-3:]}')

# regions = {"CH": 1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1, "CHS": 3}
# season_names = { "JJA": 'Summer',"DJF": 'Winter', "MAM": 'Spring',
#                 "SON": 'Autumn'}

# Institutes = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom']

for season in season_names.keys():
    ensemble_list, cc_ensemble_list = [], []
    tas_list = []
    print((season).center(50,'-'))
  
    for cpm in Institutes:
    
        print('Individual:' + cpm)

        pr_rcp = xr.open_dataset(path_pr +
                f'/Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
            # f'/OneYearStep_Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
        pr_hist = xr.open_dataset(path_pr_hist +
                f'/Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
            # f'/OneYearStep_Regridded_{cpm}_{time_res}_RP_prmax_{season}.nc')
        ds_scaling = copy.deepcopy(pr_rcp)
        ds_cc = copy.deepcopy(pr_rcp)
        ds_tas = copy.deepcopy(pr_rcp) 
          
        for region in regions.keys():
            print((region).center(50,' '))
            pr_rcp_region = pr_rcp[region]
            pr_hist_region = pr_hist[region]

            tas = xr.open_dataset(path_tas + 
                    f'/Regridded_ccsignal_{cpm}_{region}_{season}.nc')
            pr_change = \
                ((pr_rcp_region - pr_hist_region) / pr_hist_region) * 100
            
            ones = np.ones_like(pr_change)
            ones[:][:] = tas['tas_mean'].values

            scaling = pr_change / ones 

            ds_scaling[region] = scaling
            ds_cc[region] = pr_change
            ds_tas[region] = tas['tas_mean']           
            

            tas.close()
        # print(np.nanmedian(ds_cc['CHS'][10][1].values))
        # print(np.nanmedian(ds_scaling['CHS'][10][1].values))
        
        pr_rcp.close()
        pr_hist.close()

        ensemble_list.append(ds_scaling)
        cc_ensemble_list.append(ds_cc)
        tas_list.append(ds_tas)

        print(('saving stuff').center(50,' '))
        ### Add processing information
        proc_info = ('Calculation of scaling parameter i.e. the rate of '
                     +'precipitation change per degree change of temperature'
                     +f'until {scenario[-3:].capitalize()}')
        ds_scaling.attrs["processing_information"] = proc_info
        proc_info = (f'Calculation of precipitation change until {scenario[-3:].capitalize()}')
        ds_cc.attrs["processing_information"] = proc_info

        ### Save individual CPMS
        filenmae_new = path_pr + f'/{cpm}_scaling_{time_res}_RP_prmax_{season}.nc'
        # filenmae_new = path_pr + f'/OneYearStep_{cpm}_scaling_{time_res}_RP_prmax_{season}.nc'
        ds_scaling.to_netcdf(path=filenmae_new)
        filenmae_new = path_pr + f'/{cpm}_cc_{time_res}_RP_prmax_{season}.nc'
        # filenmae_new = path_pr + f'/OneYearStep_{cpm}_cc_{time_res}_RP_prmax_{season}.nc'
        ds_cc.to_netcdf(path=filenmae_new)

        ### Add processing information and save temperature
        proc_info = (f'Temperature change until {scenario[-3:].capitalize()}')
        ds_tas.attrs["processing_information"] = proc_info
        filenmae_new = path_pr + f'/{cpm}_tas_{time_res}_RP_prmax_{season}.nc'
        ds_tas.to_netcdf(path=filenmae_new)

    for i in range(len(ensemble_list)):
        print("Calculate ensemble adding:" + Institutes[i])

        def drop_unnecessary_vars(ensemble_list):
            global i
            try:
                ensemble_list[i] = ensemble_list[i].drop('rotated_pole')
                pass
            except:
                pass
            try:
                ensemble_list[i] = ensemble_list[i].drop('latitude_bnds')
                ensemble_list[i] = ensemble_list[i].drop('longitude_bnds')
            except:
                pass
            try:
                ensemble_list[i] = ensemble_list[i].drop('lon_bnds')
                ensemble_list[i] = ensemble_list[i].drop('lat_bnds')
            except:
                pass
            try:
                ensemble_list[i] = ensemble_list[i].drop('bounds_lon')
                ensemble_list[i] = ensemble_list[i].drop('bounds_lat')
            except:
                pass

            return ensemble_list

            # try:
            #     tas_list[i] = tas_list[i].drop('rotated_pole')
            #     pass
            # except:
            #     pass
            # try:
            #     tas_list[i] = tas_list[i].drop('latitude_bnds')
            #     tas_list[i] = tas_list[i].drop('longitude_bnds')
            # except:
            #     pass
            # return

        ensemble_list = drop_unnecessary_vars(ensemble_list)
        tas_list = drop_unnecessary_vars(tas_list)
        cc_ensemble_list = drop_unnecessary_vars(cc_ensemble_list)

        if i == 0:
            ensemble = copy.deepcopy(ensemble_list[i])
            tas_ensemble = copy.deepcopy(tas_list[i])
            cc_ensemble = copy.deepcopy(cc_ensemble_list[i])
        else:
            try:
                ensemble += ensemble_list[i]
                tas_ensemble += tas_list[i]
                cc_ensemble += cc_ensemble_list[i]
            except:
                breakpoint()

    ensemble = (ensemble / len(ensemble_list))
    tas_ensemble = (tas_ensemble / len(tas_list))
    cc_ensemble = (cc_ensemble / len(cc_ensemble_list))

    ### Scaling
    proc_info = ('Calculation of scaling parameter i.e. the rate of '
                +'precipitation change per degree change of temperature (Ensemble mean)')         
    ensemble.attrs["processing_information"] = proc_info
    filenmae_new = path_pr + f'/EnsembleMean_scaling_{time_res}_RP_prmax_{season}.nc'
    # filenmae_new = path_pr + f'/OneYearStep_EnsembleMean_scaling_{time_res}_RP_prmax_{season}.nc'
    ensemble.to_netcdf(path=filenmae_new)

    ### Climate Change Signal
    proc_info = (f'Calculation of precipitation change until {scenario[-3:].capitalize()} (Ensemble mean)')
    cc_ensemble.attrs["processing_information"] = proc_info
    filenmae_new = path_pr + f'/EnsembleMean_cc_{time_res}_RP_prmax_{season}.nc'
    # filenmae_new = path_pr + f'/OneYearStep_EnsembleMean_cc_{time_res}_RP_prmax_{season}.nc'
    cc_ensemble.to_netcdf(path=filenmae_new)

    ### Temperature
    proc_info = (f'Temperature change until {scenario[-3:].capitalize()}, '
                 + 'ensemble mean temperature')      
    tas_ensemble.attrs["processing_information"] = proc_info
    filenmae_new = path_pr + f'/EnsembleMean_tas_{time_res}_RP_prmax_{season}.nc'
    tas_ensemble.to_netcdf(path=filenmae_new)