#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
November 2023

Authors:
- Leandro Gimmi

Description:
Calculate multimodel mean from available model ensemble for every returnperiod

"""

import copy
import warnings
import numpy as np
import xesmf as xe
import xarray as xr 
warnings.simplefilter("ignore")


'''-------------------------------------------------------------------------'''

### Choose between evaluation / historical / rcp85_moc / rcp85_eoc
scenario = 'rcp85_eoc'

### xHourly, xDaily (1hr,1d)
time_res = '1d'

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)
return_periods = np.insert(return_periods,0,2)
return_periods = np.append(return_periods, [150, 200, 250, 300])
# return_periods = [2,5,10,20,30,50,100,200,300]

seasons = ['JJA', 'DJF', 'SON', 'MAM']
regions = {"CH":1,"CHAE": 4, "CHAW": 5, "CHW": 2, "CHNE": 1, "CHS": 3}
season_names = {"DJF": 'Winter', "JJA": 'Summer', "MAM": 'Spring',
                "SON": 'Autumn'}
stats = ['Mean','Median','10P','90P']

'''-------------------------------------------------------------------------'''
### - - - - - - - - - - - - - - - -  CPM  - - - - - - - - - - - - - - - - - - -
path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/tas/cc_signal/'
             +f'{scenario}')
# path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/{scenario}')

# # Institutes = ['CLMcom-ETH','CLMcom-BTU','CLMcom-JLU',
# #               'CLMcom-KIT','KNMI','HCLIMcom','MOHC']
# # CPMs = ['COSMO-crCLIM','CCLM5-0-14','CCLM5-0-15','CCLM5-0-15',
# #         'HCLIM38h1-AROME','HCLIM38-AROME','HadREM3-RA-UM10.1']
# Institutes = ['CLMcom-BTU','CLMcom-ETH','CLMcom-JLU', 'CLMcom-CMCC'
#               'CLMcom-KIT','CNRM','KNMI','HCLIMcom','MOHC','ICTP']
# Institutes = ['CLMcom-CMCC']
Institutes = ['CLMcom-ETH','CLMcom-BTU','CLMcom-CMCC',
              'CLMcom-JLU', 'CLMcom-KIT','CNRM','KNMI','HCLIMcom']
rcm = None

### - - - - - - - - - - - - - - - -  RCM  - - - - - - - - - - - - - - - - - - -
# path_data = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
#              +f'{scenario}')
# path_data = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/{scenario}')

# if scenario == 'evaluation' and time_res == '1hr':
#     Institutes = ['CLMcom-ETH','CLMcom-BTU','KNMI','HCLIMcom']
#     CPMs = ['COSMO-crCLIM','CCLM5-0-9','RACMO23E','HCLIM38-ALADIN']
# elif scenario == 'evaluation' and time_res == '1d':
#     Institutes = ['KNMI','HCLIMcom']
#     CPMs = ['RACMO23E','HCLIM38-ALADIN']
# else:
#     Institutes = ['CLMcom-ETH','KNMI','HCLIMcom']
#     CPMs = ['COSMO-crCLIM','RACMO23E','HCLIM38-ALADIN']
# Institutes = ['CLMcom-BTU','CLMcom-ETH',
#               'KNMI','HCLIMcom','ICTP']


# rcm = 1

'''-------------------------------------------------------------------------'''
if scenario == 'rcp85_moc':
    path_data = path_data[:-9] + '/moc'
elif scenario == 'rcp85_eoc':
    path_data = path_data[:-9] + '/eoc'


for i_seas, season in enumerate(seasons):
    print((season.center(60,'-')))

    ### Regridding temperature change signal to grid of first CPM in list (ETH)
    for region in regions:
        for i_cpm, cpm in enumerate(Institutes):
            filename = path_data + f'/ccsignal_{cpm}_{region}_{season}.nc'
            ds = xr.open_dataset(filename)
           
            if i_cpm == 0:
                target_grid, target_name = copy.deepcopy(ds), cpm
                filename_new = (path_data + 
                    f'/Regridded_ccsignal_{cpm}_{region}_{season}.nc')
                ds.to_netcdf(path=filename_new)
            else:
                breakpoint()
                regridder = xe.Regridder(ds, target_grid, "bilinear")
                print(f'Regridding {cpm} to {target_name} for multimodel mean')
                ds_regridded = regridder(ds)
                filename_new = (path_data + 
                    f'/Regridded_ccsignal_{cpm}_{region}_{season}.nc')
                ds_regridded.to_netcdf(path=filename_new)
                

            ds.close()

    ### Regridding all models to the grid of the stations for reresentative map
    ### in multi CPM plots vs Obs with map of region + Station location (precip)
    # path_region_stations = '/scratch/snx3000/lgimmi/store/ALP-3/masks'
    # for i_cpm, cpm in enumerate(Institutes):
    #     filename = path_data + f'/{cpm}_{time_res}_RP_prmax_{season}.nc'
    #     ds = xr.open_dataset(filename)

    #     target_name = (path_region_stations + 
    #                         '/station_region_latlon_maskCH.nc')
    #                         # '/station_region_latlon.nc')
    #     target_grid = xr.open_dataset(target_name)

    #     if cpm == 'CLMcom-CMCC':
    #         ### All of CH Mask file of CMCC is wrongly labeled
    #         try:
    #             ds = ds.rename({'x':'rlon','y':'rlat'})
    #         except:
    #             pass

    #     ### Regridd models
    #     regridder = xe.Regridder(ds, target_grid, "bilinear")
    #     print(f'Regridding {cpm}')
    #     ds_regridded = regridder(ds)
    #     filename_new = (path_data + 
    #         f'/StatRegrid_CH_{cpm}_{time_res}_RP_prmax_{season}.nc')
    #         # f'/StatRegrid_{cpm}_{time_res}_RP_prmax_{season}.nc')
    #     ds_regridded.to_netcdf(path=filename_new)
            

    #     ds.close()