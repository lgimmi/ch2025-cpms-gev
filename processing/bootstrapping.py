#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
September 2023

Authors:
- Leandro Gimmi

Description:
Non-parametric bootstrapping approach applied to maxima block data to generate 
50 bootstrap samples. 

"""
import os
import glob
import time
import warnings
warnings.simplefilter("ignore")
import numpy as np
import xarray as xr 
import pandas as pd
import multiprocessing as mp
from natsort import natsorted
import matplotlib.pyplot as plt
import rpy2.robjects as robjects
from scipy.stats import bootstrap
from rpy2.robjects import numpy2ri
from rpy2.robjects import pandas2ri
from rpy2.robjects.packages import importr
from multiprocessing import Pool

'''-------------------------------------------------------------------------'''

### Choose between evaluation / historical / rcp85_moc / rcp85_eoc
scenario = 'historical'

### xHourly (1hr,3hr,6hr), xDaily (1d,3d,5d)
time_res = '1d'

### Define Returnperiods for which values GEV values should be calculated
return_periods = np.arange(5,105,5)
return_periods = np.insert(return_periods,0,2)
return_periods = np.append(return_periods, [150, 200, 250, 300])
# return_periods = np.arange(2,201,1)

### If the season or the region should be specified please refer to the end of 
### the script and adjust for your liking. Generally all seasons and all 
### CH2018 regions (CHNE, CHW, CHS ,CHAE and CHAW) are used.

### ------------------------------- CPM ----------------------------------- ###
plot_path = '/users/lgimmi/MeteoSwiss/figures'
path_data = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_maxima/'
             +f'{scenario}')
path_save = (f'/scratch/snx3000/lgimmi/store/ALP-3/{time_res}/pr_GEV/'
             +f'{scenario}')

Institutes = ['CLMcom-BTU','CLMcom-CMCC','CLMcom-ETH','CLMcom-JLU',
              'CLMcom-KIT','CNRM','KNMI','HCLIMcom','MOHC','ICTP']
CPMs = ['CCLM5-0-14','CCLM5-0-9','COSMO-crCLIM','CCLM5-0-15','CCLM5-0-15',
        'AROME41t1','HCLIM38h1-AROME','HCLIM38-AROME','HadREM3-RA-UM10.1',
        'RegCM4-7']

rcm = None
# 
### ------------------------------- RCM ----------------------------------- ###
# plot_path = '/users/lgimmi/MeteoSwiss/figures'
# path_data = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_maxima/'
#              +f'{scenario}')
# path_save = (f'/scratch/snx3000/lgimmi/store/rcm/{time_res}/pr_GEV/'
#              +f'{scenario}')

# if scenario == 'evaluation' and time_res == '1hr':
#     Institutes = ['CLMcom-BTU','CLMcom-ETH',,'KNMI','HCLIMcom','ICTP']
#     CPMs = ['CCLM5-0-9','COSMO-crCLIM','ALADIN62','RACMO23E','HCLIM38-ALADIN',
#             'RegCM4-7']
# elif scenario == 'evaluation' and time_res == '1d':
#     Institutes = ['CLMcom-ETH','CLMcom-BTU','CNRM','KNMI','HCLIMcom','ICTP']
#     CPMs = ['COSMO-crCLIM','CCLM5-0-9','ALADIN62','RACMO23E','HCLIM38-ALADIN',
#             'RegCM4-7']
# else:
#     Institutes = ['CLMcom-ETH','CNRM','KNMI','HCLIMcom','ICTP']
#     CPMs = ['COSMO-crCLIM','ALADIN63','RACMO23E','HCLIM38-ALADIN',
#             'RegCM4-7']

# rcm = 1
'''-------------------------------------------------------------------------'''

if scenario == 'rcp85_moc':
    path_data = path_data[:-4] + '/moc'
    path_save = path_save[:-4] + '/moc'
elif scenario == 'rcp85_eoc':
    path_data = path_data[:-4] + '/eoc'
    path_save = path_save[:-4] + '/eoc'

### Store erroreous gridcell information to analyse severity
df_err = pd.DataFrame(columns=['scenario', 'region', 'season', 'model', 'row',
                                'column', 'iteration', 'safety'])

### Automatically translates python np.arrays and pd.datframes to R style
numpy2ri.activate()
pandas2ri.activate()

### Needed to install 'gevXpgd' module as it is not a standard package 
### included in rpy2
if not robjects.packages.isinstalled("gevXgpd"):
    utils = importr("utils")
    utils.install_packages("remotes", repos="https://cloud.r-project.org")
    remotes = importr("remotes")
    remotes.install_github("C2SM/gevXgpd")

### import R module as variable -> can now be used like a common function
gevXgpd = importr("gevXgpd")

# '''-------------------------------------------------------------------'''
### Run GEV for individual gridcell maxima with additional 50 bootstrap
### maxima arrays  
# '''-------------------------------------------------------------------'''
def bootstrap_gridcells(identifier, cpm):
    '''
    Returns GEV return period values for individual gridcell maxima with
    additional 50 bootstrap maxima arrays.

    '''
    n_bootstraps = 50
    filename = (path_data + '/' + f'maxima_{cpm}_{region}_{season}.npy')

    data = np.load(filename, allow_pickle=True)
    empty_array = np.empty((data.shape[1],data.shape[2], 50)).tolist()
    t_beg = time.time()
    np.random.seed(7) 
    for column in range(data.shape[2]):
        for row in range(data.shape[1]):
            gridcell_og = data[:,row,column]
            if all(pd.isnull(gridcell_og)):
                continue
            for iteration in range(n_bootstraps):
                # print((f'{row}, {column}, {iteration}').center(40,'-'))
                # if row == 0 and column == 7 and iteration == 16:
                #     breakpoint()
                GEV_output = None
                safety = 0
                while GEV_output is None and safety <= 10:
                    ### Somtimes np.random_choice creates singluar matrix
                    ### systems which won't work for GEV function -> redo
                    try:
                        safety+=1
                        if iteration == 0:
                            gridcell = gridcell_og
                        else:
                            gridcell = np.random.choice(gridcell_og,size=30)
                
                        ### Remove nan values as gevXgpd cannot take nan values
                        gridcell = gridcell[np.logical_not(pd.isnull(gridcell))]
                        if len(gridcell) == 0:
                            breakpoint()
                            break

                        ### Input has to be 1-D df with 'numeric' type column
                        df = pd.DataFrame(gridcell, columns=['Column1'])
                        df["Column1"] = pd.to_numeric(df["Column1"])

                        ### Run GEV function
                        GEV_output = gevXgpd.fitGEV(df,ret=return_periods)
                    except:
                        global err_data
                        global df_err
                        err_data = [f'{scenario}',f'{region}',f'{season}',
                                    f'{cpm}',f'{row}',f'{column}',
                                    f'{iteration}',f'{safety}']
                        df_err.loc[len(df_err)] = err_data
                        print('redo')
                        
                if safety >= 10:
                    continue
                    
                ### Get values for return period from output
                empty_array[row][column][iteration] = GEV_output[5]
                try:
                    if GEV_output[5][0][1] < 0:
                        print('Negative Value')
                        breakpoint()
                except TypeError:
                    pass
    
    print(f'Time to calculate GEV for {cpm}: ' + 
                            '%.2f' % (time.time() - t_beg) + ' s')
    try:
        rm_old_file = ('rm ' + path_save + 
            f'/GEV_{cpm}_{region}_{season}.npy')
        os.system(rm_old_file)
    except:
        print('No old file to remove')

    np.save(path_save + '/' + f'GEV_{cpm}_{region}_{season}.npy',
            np.array(empty_array, dtype=object), allow_pickle=True)

    # try:
    #     rm_old_file = ('rm ' + path_save + 
    #         f'/OneYearStep_GEV_{cpm}_{region}_{season}.npy')
    #     os.system(rm_old_file)
    # except:
    #     print('No old file to remove')

    # np.save(path_save + '/' + f'OneYearStep_GEV_{cpm}_{region}_{season}.npy',
    #         np.array(empty_array, dtype=object), allow_pickle=True)
    return identifier
        

for region in ['CH', 'CHNE', 'CHW', 'CHS' ,'CHAE', 'CHAW']:
    for season in ['DJF', 'MAM', 'JJA', 'SON']:
        print((season).center(60,'~'))
        # for cpm in Institutes:
        #     bootstrap_gridcells(1,cpm)

        with Pool() as pool:
            pool.starmap(bootstrap_gridcells, enumerate(Institutes))


